##Tactics
The tactics are called by the loader to determine the behavior of each agent. A new tactics object is initialized for each agent. 
####Initialize
This method is called upon the initialization of the agent. I have not explored the other facets of this method.
####Precompute
This method is involved with the precomputation step of the simulator if one is given. I have not explored precomputation yet.
####Resume
I have not explored this method, but I would assume it is related to resuming a simulation after stopping it.
####Preparate
I have not explored this method.
####Think
This method is called on each timestep of the simulation to determine what the agent is going to do. It must return a valid action for that agent. I would recommend never returning null in this method, instead opting to return an ActionRest.

####Parameters
######AgentInfo
This gives information on the agent. To get the StandardEntity of the agent, call `agentInfo.me()`. I would recommend casting that output to whatever type of entity the tactics class is for (I.E. police force).
######WorldInfo
This object holds what the agent knows about the world. It starts out knowing where all the roads and buildings are I believe. According to the literature, it is updated automatically when the agent finds something new. It does not change automatically when something is removed or changed. I have not explored to the extent to which that works. Two notable methods are `worldInfo.getRawWorld()` which returns the full map with nothing on it and `worldInfo.getEntity(EntityID id)` which returns a StandardEntity given its ID. 
######ScenarioInfo
This object holds the scenario-specific information. This pertains to qualities such as the range of the police's clearing or how much water the fire man can hold.
######ModuleManager
This object is in charge of fetching class declarations from various modules. I do not see the reason to use this object over normal object instantiation.
######MessageManager
This object is in charge of the communication between agents. It contains a list of the received communications and is added to send messages to other agents. 
######DevelopData
I have no idea what this object is for.
