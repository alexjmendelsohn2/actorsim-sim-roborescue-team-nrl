
    exploredRoads = empty set
    exploredBuildings = empty set
    buildingsOnFire = empty set
    roadsWithBlockades = empty set
    stuckAgents = empty set
    stuckCivilians = empty set
    position = null;
    
    while isAlive:
    
    	position = id of the area that the police is in	
    	updateInformationFromCommunications(exploredRoads, exploredBuildings, roadsWithBlockades, buildingsOnFire)
    	sendInformationUpdates
    	action = null
    	
    	if commanded:
    		
    		action = commandedAction if possible
    		sendMessage(report on progress)
    	
    	if action = null:
    			
    		if position contains a stuck agent:
    				
    				action = clearRoad (position)
    				
			else if there are still stuck agents:
				
				path = shortest path to closest stuck agent
				
				if path has blockades:
					
					if blockade in range:
						
						action = actionClear(blockade)
						
					else:
					
						path = shortest path to closest blockade along the previous path
						action = actionMove(path)
						
				else:
					
					action = actionMove(path)
					
			else if position contains a stuck civlian:
    				
    				action = clearRoad (position)
					
			else if there are still stuck civilians:
			
				path = shortest path to closest stuck civilian
				
				if path has blockades:
					
					if blockade in range:
						
						action = actionClear(blockade)
						
					else:
					
						path = shortest path to closest blockade along the previous path
						action = actionMove(path)
						
				else:
					
					action = actionMove(path)
					
			else:
			
				if neighbor is building and road has blockades:
				
					action = clearRoad(position)
					
				else if there are nearby buildings that are blocked:
				
					if nearby building has a blockade in range:
					
						action = actionClear(blockade)
						
					else:
						
						path = path to get closest to blocked building as possible
						action = actionMove(path)
						
				else if there are still roads with blockades:
				
					if position is in roadsWithBlockades:
					
						action = clearRoad(position);
						
					else:
					
						if blockade in range:
						
							action = actionClear(blockade)
							
						else:
						
							path = shortest path to closest blockaded road
							action = actionMove(path)
							
				else if not all roads are explored:
				
					path = shortest path to closest unexplored roads
					action = actionMove(path)
					
				else if not all not on fire buildings are explored:
				
					path = shortest path to unexplored building not on fire
					action = actionMove(path)
				
				else:
					
					action = actionRest
					
    	sendMessage(what police is going to do)
    	do action
				
				
			
				
