
	exploredRoads = empty set
	buildingsOnFire = empty set
	buildingsWithBuriedPeople = empty set
	position = null
	
	while isAlive:
		
		position = id of the area that the fire brigade is in	
    	updateInformationFromCommunications(exploredRoads, buildingsOnFire, buildingsWithBuriedPeople)
    	sendInformationUpdates
    	action = null;	
    	
    	if commanded:
    		
    		action = commandedAction if possible
    		sendMessage(report on progress)
    	
    	if action = null:
    		
    		if needsRefill:
    			
    			if atHydrant or inRefuge:
    				
    				action = actionRefill
    				
    			else:
    				
    				path = the shortest path to the closest hydrant/refuge
    				action = actionMove(path)
    				
    		else if buildingsOnFire is not empty:
    		
    			if there are buildings on fire with buried people:
    				
    				fireBuilding = closest building on fire with buried people
    				
    				if fireBuilding is in range:
    					
    					action = actionExinguish(fireBuilding)
    					
    				else:
    				
    					path = the shortest path to fireBuilding
    					action = actionMove(path)
    					
    			else:
    			
    				fireBuilding = closest building on fire
    				
    				if fireBuilding is in range:
    					
    					action = actionExinguish(fireBuilding)
    					
    				else:
    				
    					path = the shortest path to fireBuilding
    					action = actionMove(path)
    		
    		else:
    		
    			if there are unexplored roads:
    				
    				path = the shortest path to the closest unexplored road
    				action = actionMove(path)
    				
    			else if tankIsNotFull:
    				
    				if atHydrant or inRefuge:
    				
    					action = actionRefill
    				
    				else:
    				
    					path = the shortest path to the closest hydrant/refuge
    					action = actionMove(path)
    					
    			else:
    				resetUnexploredRoads
    				action = actionRest
    		
    	sendMessage(what fire brigade is going to do)
    	do action
    			
    			
    		