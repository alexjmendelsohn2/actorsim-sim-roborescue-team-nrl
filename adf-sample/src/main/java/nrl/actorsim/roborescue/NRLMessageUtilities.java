package nrl.actorsim.roborescue;

import adf.agent.action.Action;
import adf.agent.action.ambulance.ActionLoad;
import adf.agent.action.ambulance.ActionRescue;
import adf.agent.action.ambulance.ActionUnload;
import adf.agent.action.common.ActionMove;
import adf.agent.action.common.ActionRest;
import adf.agent.action.fire.ActionExtinguish;
import adf.agent.action.fire.ActionRefill;
import adf.agent.action.police.ActionClear;
import adf.agent.communication.standard.bundle.centralized.CommandAmbulance;
import adf.agent.communication.standard.bundle.centralized.CommandFire;
import adf.agent.communication.standard.bundle.centralized.CommandPolice;
import adf.agent.communication.standard.bundle.centralized.MessageReport;
import adf.agent.communication.standard.bundle.information.*;
import adf.component.communication.CommunicationMessage;

public class NRLMessageUtilities {
	public static int translateActionToActionIndex(Class<? extends Action> actionClass) {
		if(actionClass == ActionRest.class) {
			return MessageAmbulanceTeam.ACTION_REST;
		}
		if(actionClass == ActionMove.class) {
			return MessageAmbulanceTeam.ACTION_MOVE;
		}
		if(actionClass == ActionRescue.class) {
			return MessageAmbulanceTeam.ACTION_RESCUE;
		}
		if(actionClass == ActionLoad.class) {
			return MessageAmbulanceTeam.ACTION_LOAD;
		}
		if(actionClass == ActionUnload.class) {
			return MessageAmbulanceTeam.ACTION_UNLOAD;
		}
		if(actionClass == ActionClear.class) {
			return MessagePoliceForce.ACTION_CLEAR;
		}
		if(actionClass == ActionExtinguish.class) {
			return MessageFireBrigade.ACTION_EXTINGUISH;
		}
		if(actionClass == ActionRefill.class) {
			return MessageFireBrigade.ACTION_REFILL;
		}
		return -1;
	}

	public static String toString(CommunicationMessage message) {
		String result = "";
        if (message instanceof MessageRoad) {
            MessageRoad roadMsg = (MessageRoad) message;
            result = String.format("ROAD      id:%s passable:%s", roadMsg.getRoadID(), roadMsg.isPassable());
        } else if (message instanceof MessageBuilding) {
            MessageBuilding bMsg = (MessageBuilding) message;
            result = String.format("BUILDING  id:%s broken:%s fire:%s temp:%s",
                    bMsg.getBuildingID(), bMsg.getBrokenness(), bMsg.getFieryness(), bMsg.getTemperature());
        } else if (message instanceof MessageAmbulanceTeam) {
            MessageAmbulanceTeam actMsg = (MessageAmbulanceTeam) message;
            result = String.format("AMBULANCE id:%s pos:%s target:%s  buried:%s damaged:%s",
                    actMsg.getAgentID(), actMsg.getPosition(), actMsg.getTargetID(), actMsg.getBuriedness(), actMsg.getDamage());
        } else if (message instanceof MessagePoliceForce) {
            MessagePoliceForce actMsg = (MessagePoliceForce) message;
            result = String.format("POLICE    id:%s pos:%s target:()  buried:%s damaged:%s",
                    actMsg.getAgentID(), actMsg.getPosition(), actMsg.getTargetID(), actMsg.getBuriedness(), actMsg.getDamage());
        } else if (message instanceof MessageFireBrigade) {
            MessageFireBrigade actMsg = (MessageFireBrigade) message;
            result = String.format("BRIGADE   id:%s pos:%s target:()  buried:%s damaged:%s",
                    actMsg.getAgentID(), actMsg.getPosition(), actMsg.getTargetID(), actMsg.getBuriedness(), actMsg.getDamage());
        } else if (message instanceof MessageCivilian) {
            MessageCivilian actMsg = (MessageCivilian) message;
            result = String.format("CIVILIAN  id:%s pos:%s buried:%s damaged:%s",
                    actMsg.getAgentID(), actMsg.getPosition(), actMsg.getBuriedness(), actMsg.getDamage());
        } else if (message instanceof MessageReport) {
            MessageReport reportMsg = (MessageReport) message;
            result = String.format("REPORT    agent:%s done:%s failed:%s",
                    reportMsg.getFromID(), reportMsg.isDone(), reportMsg.isFailed());
        } else if (message instanceof CommandAmbulance) {
            CommandAmbulance cmdMsg = (CommandAmbulance) message;
            result = String.format("COMMAND    agent:%s command:%s target:%s",
                    cmdMsg.getToID(), ambulanceAction(cmdMsg.getAction()), cmdMsg.getTargetID());
        } else if (message instanceof CommandFire) {
            CommandFire cmdMsg = (CommandFire) message;
            result = String.format("COMMAND    agent:%s command:%s target:%s",
                    cmdMsg.getToID(), fireAction(cmdMsg.getAction()), cmdMsg.getTargetID());
        } else if (message instanceof CommandPolice) {
            CommandPolice cmdMsg = (CommandPolice) message;
            result = String.format("COMMAND    agent:%s command:%s target:%s",
                    cmdMsg.getToID(), policeAction(cmdMsg.getAction()), cmdMsg.getTargetID());
        } else {
            result = String.format("UNKNOWN %s", message);
        }
        return result;
    }

	public static String ambulanceAction(int action) {
		switch(action) {
			case CommandAmbulance.ACTION_REST:
				return "REST";
			case CommandAmbulance.ACTION_MOVE:
				return "MOVE";
			case CommandAmbulance.ACTION_RESCUE:
				return "RESCUE";
			case CommandAmbulance.ACTION_LOAD:
				return "LOAD";
			case CommandAmbulance.ACTION_UNLOAD:
				return "UNLOAD";
			case CommandAmbulance.ACTION_AUTONOMY:
				return "AUTONOMY";
			default:
				return "UNKNOWN";
		}
	}

	public static String fireAction(int action) {
		switch (action) {
			case CommandFire.ACTION_REST:
				return "REST";
			case CommandFire.ACTION_MOVE:
				return "MOVE";
			case CommandFire.ACTION_EXTINGUISH:
				return "EXTENGUISH";
			case CommandFire.ACTION_REFILL:
				return "REFILL";
			case CommandFire.ACTION_AUTONOMY:
				return "AUTONOMY";
			default:
				return "UNKNOWN";
		}
	}

	public static String policeAction(int action) {
		switch (action) {
			case CommandPolice.ACTION_REST:
				return "REST";
			case CommandPolice.ACTION_MOVE:
				return "MOVE";
			case CommandPolice.ACTION_CLEAR:
				return "CLEAR";
			case CommandPolice.ACTION_AUTONOMY:
				return "AUTONOMY";
			default:
				return "UNKNOWN";
		}
	}

	public static String toString(Action action) {
		return (action == null) ? "null" : action.toString();
	}

}
