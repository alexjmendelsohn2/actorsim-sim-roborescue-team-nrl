# The RoboRescue Connector Environment

The RoboRescue (RR) simulator has two main components: the server (**rr-server**) 
and the client (**rr-team-nrl**).  You only need to setup IntelliJ projects
for the components that you will actually develop against. Most developers only
 need to setup a client development environment. 

If you are developing client agents, you will need to:
- ensure that the server can run from the command line
- set up an IntelliJ project for developing client agents
  - this project will run a shell script that runs rr-server automatically.

If you are developing server code, you will also need to:
- ensure that the server can run from the command line
- set up an IntelliJ project for developing the server
- set up an IntelliJ project for developing client agents


This guide will help you get rr-server and rr-team-nrl set up as two individual
projects on IntelliJ. 


## Formatting Issues

Web browsers may not properly display the code segments in this file, and 
you will likely observe formatting issues for many of the markdown primitives 
such as lists and code blocks.  There are several solutions:
1. Open the file as raw text.  If you are in bit bucket, click the `...` 
just above the file view on the right side.  Then click `Open raw`.   
1. install a markdown reader for your browser
1. Open the file inside of an IntelliJ product that has the markdown plugin 
installed.  (This option is only available if you have installed IntelliJ for 
another project.)


******************************************************************************
## Prerequisites and Platforms

The steps to set up this connector for ActorSim involve:
- (Optional) Installing a supercharged shell environment
- Installing git and gradlw on your system
- Installing Java OpenJDK 8.0 for your system
- Cloning the source directories
- Building and testing the server
- Installing the IntelliJ IDEA, into which you will also install: 
- Running a test example inside of IDEA

At the moment, we only support Ubuntu 16. These instructions have been tested
on Ubuntu 16 and 18. We assume it should work similarly for other Unix platforms.

We have also tested this development environment on Windows 10 with the 
Windows Subsystem for Linux versions 1 or 2 using Ubuntu 16.  This setup
requires installing everything in the WSL system rather than 
installing directly into windows and is much simpler than a windows install. 
If you are running a windows machine, we strongly recommend using WSL/2.

We have explored running the server in a docker image, but it is 
challenging to run the server from a docker because it has a GUI.
If you are aware of ways to use the server in a docker image, 
please discuss this with the team at our next release planning meeting.


##### Common Issues
- I don't want to use Ubuntu16 or IntelliJ.  Can I use X instead?  (where X is some other IDE).
  - In short, not really.  We have such limited time as it is that 
    supporting one Platform/IDE is challenging enough. Ubuntu 16.04
    is in the middle of its lifecycle and fairly stable. IntelliJ 
    products support a vibrant ecosystem of plugins that will
    ease your transition.
  - However, we have recently switched to a Maven build structure, which should
    make it much easier for people to install in any IDE.  Just be aware, 
    we'll probably ask you to reproduce any problem in the standard setup first.
- [Add your problem and solution here using Markdown formatting] 



*****************************************************************************
## (Optional) System: Install Zsh
Although it is not required, Mak highly recommends the zsh environment with a variety
of excellent plugins including oh-my-zsh.  If you're interested, ask Mak for a demo 
of zsh setup and for his config files. 


*****************************************************************************
## System: Install Java OpenJDK 8.0


#### Ubuntu Setup
If you expect to be installing many varieties of Java, you may want to install
via SDKMan: https://sdkman.io/

Otherwise, simply follow the instructions for installing the latest version of
OpenJDK 8.0 on Ubuntu.  


#### Mac Setup
On a Mac, you can install this using homebrew:
```    
brew cask install java8
```

Then check to make sure that it is the right version (see above). If not, 
let one of us know and we'll dig up the specific formula or it.

##### Common Issues
- [Add your problem and solution here using Markdown formatting] 


*****************************************************************************
## Clone the source
To simplify and unify our development environments across many projects
**please** set up your directories using these recommended paths.

If you have been asked to use an NRL internal repository, please use the  
URLS provided to you separately.  Otherwise, the public repositories 
you will need to access include: 
- https://bitbucket.org/makro-nrl2/actorsim-core/src/develop/  
  Note the makro-nrl2 instead of makro-nrl.
- https://bitbucket.org/makro-nrl/actorsim-sim-roborescue-team-nrl/src/develop/
- https://bitbucket.org/makro-nrl/actorsim-sim-roborescue-server/branch/develop/
 
Let <RR_BASE_DIR> represent the root directory for this connector on your system.
<RR_BASE_DIR> is in brackets because it is not an actual variable
we will set up for IntelliJ.
This directory can be anywhere you like, but keep in mind that you may want
to have a short path because you will be required to type _absolute_ paths
 during the IDE setup.  Shown below are the recommended paths.
 
  ![Recommended Directory Structure](_images/IDEA_RR_RecommendedDirectoryStructure.png)

which equates to the following table: 

| VARIABLE NAME     | Local Directory           | Description  |
| :------------- |:------------- | :-----|
| <RR_BASE_DIR>   | `~/git-workspace/roborescue`  | Your base install directory |
| RR_ACTORSIM_CORE | `<RR_BASE_DIR>/actorsim-core-git` | ActorSim's core goal refinement library |
| RR_SERVER   | `<RR_BASE_DIR>/actorsim-sim-roborescue-server-git` | The RoboRescue Server |
| RR_TEAM_NRL | `<RR_BASE_DIR>/team-nrl-git` | The Team NRL Client Code |


These are not shell variables, so commands below are representative
of the commands you will type. 

Henceforth, we will refer to these directories by the variable name 
and you will need to replace them as appropriate.

Set up a common workspace for this connector:
- ```shell script
  cd ~
  mkdir -p <RR_BASE_DIR>
  ```

*****************************************************************************
## Clone the source
Your git repository access URL will differ based on your credentials and will be
emailed to you by a development lead.

*Note the use of the suffix `-git` for local repositories.  This is a handy way
to visually detect local repos on your system, since the .git directory 
is often hidden!  (Git best practices preclude using a `.git` suffix
 because that is reserved for a bare repository on a remote server.)*

Clone and checkout the develop branch of ActorSim core:
- ```shell script
  cd <RR_BASE_DIR> 
  git clone [URL.. actorsim-core.git]  actorsim-core-git 
  cd  actorsim-core-git
  git checkout develop #in zsh with the git plugin: gcd
  ```

Clone and checkout the develop branch of the RR server
- ```shell script
  cd <RR_BASE_DIR> 
  git clone [URL.. actorsim-sim-sim-roborescue-server.git]  actorsim-sim-roborescue-server-git 
  cd  actorsim-sim-roborescue-server-git 
  git checkout develop #in zsh with the git plugin: gcd
  ```

Clone and checkout the develop branch of the RR team code
- ```shell script
  cd <RR_BASE_DIR> 
  git clone [URL.. actorsim-sim-sim-roborescue-team-nrl.git]  actorsim-sim-roborescue-team-nrl-git 
  cd  actorsim-sim-roborescue-server-git 
  git checkout develop #in zsh with the git plugin: gcd
  ```

##### Common Issues
- [Add your problem and solution here using Markdown formatting] 


*****************************************************************************
## Build rr-server at the command line
The server source is built using **Gradle** scripts. 
Perform this step to ensure the server is running on your machine outside of 
IntelliJ.

Navigate to rr-server-git directory and run the following at the command line:
- ```
  cd RR_SERVER
  ./gradlew
  ./gradlew completeBuild
  ```
If the build is successful:
- ```
  cd ./boot
  chmod -R g+w ./     #grant application access to write files
  ./start.sh ../maps/gml/test
  ```
You should see the following image appear.
Click the Step button a few times to see the server progress the agents.

![Server Test Map](_images/RR_TestMap.png)
##### Common Issues
- [Add your problem and solution here using Markdown formatting] 


*****************************************************************************
## Setup IntelliJ IDEA by importing a project
Now that the server works on its own, you are ready to set up a project.
We've created a project template for you to import.

### Set up the Path variables for the project.
1. Open the IDEA Global Settings in one of two ways
   - If you already have another project open, then open `File > Settings` and search for `variables`  
   - If you are at the project management screen, select `Configure > Settings`
1. Add RR_ACTORSIM_CORE and select the RR_ACTORSIM_CORE path
1. Add RR_SERVER and select the RR_SERVER path
1. Add RR_TEAM_NRL and select the RR_TEAM_NRL path
   ![Setup Path Variables](_images/IDEA_RR_PathVariables.png)

### Open the project

1. Select `Open` 
1. Navigate to`TEAM_NRL_HOME/idea-projects/project-actorsim-roborescue` and click `OK`
   ![Open Project](_images/IDEA_RR_OpenProject.png)

### Reimport the maven project
1. Select the Maven tab on the right side of your window 
1. Click the 'refresh' button

   ![Reimport Maven Projects](_images/IDEA_RR_MavenImport.png)

The resulting project structure -- in the tab all the way to the left -- should 
be similar to the following image. Note the bold modules.

   ![Expected Project Structure](_images/IDEA_RR_ProjectStructure.png)

### Install the ADF Library project

This step will install several dependencies into your local Maven repo, which 
is usually stored in ~/.m2.

1. Open the Maven tab on the right side of your window
1. Open `adf-library > Lifecycle`
1. Right-click `install` and select `Run Maven Build`

This should perform a bunch of installations of jar files and report
`BUILD SUCCESS`

### Confirm the server works

The default project provides several run configurations that you can use.
These need to be checked for accuracy because sometimes you will 
need to manually create these for your system; not all project settings
transfer between systems.

Open the run configurations panel in one of two ways:
 - Select `Run > Edit Configurations`, or  
 - Along the top bar, you should see the interface for Run Configurations 
   (![](_images/IDEA_RR_RunConfigInterface.png)).  Click the drop-down box
   and select `Edit Configurations`

Select `RR Server Test5` Run Configuration and ensure it looks like the following.
**Be sure to check `Allow Parallel Run`**


   ![](_images/IDEA_RR_RunConfigServerTest5.png)
   
While you're here, select the `RR Centralized Test5` and ensure it looks like the following:   

   ![](_images/IDEA_RR_RunConfigCentralizedTest5Solo.png)

Close the run configurations panel.

Now run the `RR Server Test5` configuration.  You should see  a similar
window to what you saw when running the server from the command line.
Leave this running and continue to the next step.

### Confirm the test clients work

Leave the server from the previous step running and additionally
run `RR Centralized Test5` an additional goal window should appear.

If you do not see a goal window, check the console log for the following 
output:

```
[ RCRS ADF Version 2.3.0 (build 20180514T081132Z) ]
[INFO  ] *** DEBUG MODE ***
[START ] Connect to server (host:localhost, port:7000)
[FINISH] Connect AmbulanceTeam (success:0)
[FINISH] Connect AmbulanceCentre (success:0)
[FINISH] Connect PoliceForce (success:0)
[FINISH] Connect FireBrigade (success:0)
[FINISH] Connect FireStation (success:0)
[FINISH] Connect PoliceOffice (success:0)
[FINISH] Done connecting to server (0 agent)
```

This output indicates that none of the agents could connect with the server.
The most likely culprit is that the server did not start or was killed
just after it started.  Make sure the server is running and try this step again.

Once you get a goal window, move to the server window and click
the `Step` button a few times. Goals should appear in the goal 
window and you should see agents start to move around.  
An example goal window is next:

   ![Goal View Test5](_images/IDEA_RR_GoalViewTest5.png)


### Running the server with each new build

Once the server and client are working independently, you can try 
running the server as part of the client start up:
1. Open the Run Configuration for `RR Centralized Test5`
1. Click the `+` next to the `Before Launch` section
1. Select `Run Another Configuration`
1. Select `RR Server Test5`
   ![](_images/IDEA_RR_RunConfigCentralizedTest5.png)

Once you save, you can run this configuration, which should start the server 
and then start the client.  

If you see the server start and then die before the client connects, you 
will need to use the workaround described below.


#### Server getting Killed?  Workaround: use an external tool

There is a bug in IntelliJ that sometimes kills the server when the client 
starts and keeps this configuration from working properly.
The workaround is to create an external tool that mimics the server setup.

   ![](_images/IDEA_RR_RunExternalToolServerTest5.png)
   
   
   