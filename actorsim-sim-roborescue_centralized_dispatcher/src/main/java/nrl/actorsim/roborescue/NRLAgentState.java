package nrl.actorsim.roborescue;

import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.information.*;
import adf.agent.info.WorldInfo;
import adf.component.communication.CommunicationMessage;
import nrl.actorsim.domain.PredicateStatement;
import nrl.actorsim.domain.StatePredicate;
import nrl.actorsim.domain.Statement;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.utils.Named;
import rescuecore2.misc.Pair;
import rescuecore2.standard.entities.*;
import rescuecore2.worldmodel.ChangeSet;
import rescuecore2.worldmodel.Entity;
import rescuecore2.worldmodel.EntityID;
import rescuecore2.worldmodel.Property;

import java.util.*;

import static rescuecore2.standard.entities.StandardEntityURN.*;


/**
 * Provides a holding place for RoboRescue-specific state information
 * as well as methods for parsing the state into the WorkingMemory.
 *
 * The contents of this class are updated directly by RoboRescue code
 * through calls to the think() methods.
 *
 * Updates to WorkingMemory are initiated by the CentralizedDispatcher.
 */
public class NRLAgentState {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NRLAgentState.class);

    private final CentralizedDispatcher dispatcher;
    private final Named parent;
    private final String parentName;
    private final NRLAgentStateOptions options;
    boolean initialized = false;

    double currentTime = 0.0;

    Map<EntityID, RRWorldObject> entityIDWorldObjectMap = new HashMap<>();


    // Map state
    Map<EntityID, List<EntityID>> neighbors = new HashMap<>();
    Set<EntityID> roads = new HashSet<>();
    Set<EntityID> buildings = new HashSet<>();
    Set<EntityID> buildingsWithHydrants = new HashSet<>();
    Set<EntityID> buildingsWithRefuge = new HashSet<>();
    Set<EntityID> exploredBuildings = new HashSet<>();
    Set<EntityID> exploredRoads = new HashSet<>();

    // General entity state
    Map<EntityID, StandardEntityURN> entityIDTypeMap = new HashMap<>();
    Map<EntityID, EntityID> entityPositions = new HashMap<>();  // a map of entityID positions by entity
    Map<EntityID, Pair<Integer, Integer>> entityLocationMap = new HashMap<>(); // a map of (x, y) locations by entity
    Set<EntityID> humans = new HashSet<>();

    // Civilian State
    Set<EntityID> civiliansStuck = new HashSet<>();
    Map<EntityID, EntityID> civiliansOnAmbulanceMap = new HashMap<>();

    // General agent state
    Map<EntityID, StandardEntityURN> agentTypeMap = new HashMap<>();
    List<StandardEntityURN> agentTypes = Arrays.asList(AMBULANCE_TEAM, POLICE_FORCE, FIRE_BRIGADE);
    Map<EntityID, Integer> humansBuried = new HashMap<>();
    Map<EntityID, Integer> humansDamaged = new HashMap<>();
    Set<EntityID> agentsStuck = new HashSet<>();

    // Ambulance State
    HashSet<EntityID> buildingsWithBuriedPeople = new HashSet<>();
    HashMap<EntityID, EntityID> injuredCiviliansNotInRefugeWithPosition = new HashMap<>();

    // Police State
    Map<EntityID, EntityID> stuckAgentsWithPositions = new HashMap<>();
	Map<EntityID, EntityID> stuckCiviliansWithPositions = new HashMap<>();
    Map<EntityID, List<Integer>> blockadesToApexesMap = new HashMap<>();
    Map<EntityID, EntityID> blockadesToLocationMap = new HashMap<>();
    Map<EntityID, Integer> blockadesWithRepairCost = new HashMap<>();
    Map<EntityID, EntityID> agentsWithLastPosition = new HashMap<>();

    // FireBrigade State
    Set<EntityID> buildingsBurning = new HashSet<>();
    Map<EntityID, Integer> brigadeAvailableWaterMap = new HashMap<>();

    public NRLAgentState(CentralizedDispatcher dispatcher, NRLAgentStateOptions options) {
        this.dispatcher = dispatcher;
        this.parent = dispatcher;
        this.parentName = dispatcher.getShortName();
        this.options = options;
    }

    public NRLAgentState(Named parent, NRLAgentStateOptions options) {
        this.dispatcher = null;
        this.parent = parent;
        this.parentName = parent.getShortName();
        this.options = options;
    }

    // ================================================================
    // region<Init>

    public void initialize(WorldInfo worldInfo) {
        if (options.initializeRoadsAndBuildingsAndAgents) {
            initializeRoadsAndBuildingsAndAgents(worldInfo);
        }
        initialized = true;
    }

    private void initializeRoadsAndBuildingsAndAgents(WorldInfo worldInfo) {
        Collection<StandardEntity> entities = worldInfo.getAllEntities();

        //add roads, areas (including buildings) first
        for (StandardEntity entity : entities) {
            if (entity instanceof Road) {
                addEntity(worldInfo, (Road) entity);
            } else if (entity instanceof Area) {
                addEntity(worldInfo, (Area) entity);
            }
        }

        //add human entities, updating explored roads and buildings
        for (StandardEntity entity : entities) {
            if (entity instanceof Human) {
                addEntity(worldInfo, (Human) entity);
            }
        }
    }

    public void addStandardEntityDetails(WorldInfo worldInfo, StandardEntity entity) {
        EntityID entityID = entity.getID();
        StandardEntityURN entityURN = entity.getStandardURN();
        entityIDTypeMap.put(entityID, entityURN);
        entityLocationMap.put(entityID, worldInfo.getLocation(entity));
    }

    private void addEntity(WorldInfo worldInfo, Road road) {
        addStandardEntityDetails(worldInfo, road);
        EntityID entityID = road.getID();
        roads.add(entityID);
        entityPositions.put(entityID, entityID);
        List<EntityID> neighborList = new ArrayList<>(road.getNeighbours());
        neighbors.put(entityID, neighborList);
    }

    private void addEntity(WorldInfo worldInfo, Area area) {
        addStandardEntityDetails(worldInfo, area);
        EntityID entityID = area.getID();
        StandardEntityURN entityURN = area.getStandardURN();

        entityPositions.put(entityID, entityID);
        List<EntityID> neighborList = new ArrayList<>(area.getNeighbours());
        neighbors.put(entityID, neighborList);
        if (area instanceof Building) {
            buildings.add(entityID);
            Building building = (Building) area;
            if (building.isOnFire()) {
                buildingsBurning.add(entityID);
            }

            if (entityURN == REFUGE) {
                buildingsWithRefuge.add(entityID);
            }
        } else if (area instanceof Hydrant) {
            buildingsWithHydrants.add(entityID);
        }
    }

    private void addEntity(WorldInfo worldInfo, Human entity) {
        addStandardEntityDetails(worldInfo, entity);
        EntityID entityID = entity.getID();
        StandardEntityURN entityURN = entity.getStandardURN();

        entityIDTypeMap.put(entityID, entityURN);
        entityLocationMap.put(entityID, worldInfo.getLocation(entity));

        humans.add(entityID);
        entityPositions.put(entityID, entity.getPosition());

        if (agentTypes.contains(entityURN)){
            agentTypeMap.put(entityID, entityURN);
            exploredRoads.add(entity.getPosition());
        }
    }
    // endregion
    // ================================================================

    // ================================================================
    // region<Update from RR WorldInfo>

    public void update(WorldInfo worldInfo, MessageManager messageManager) {

        buildingsBurning.clear();
        buildingsBurning.addAll(worldInfo.getFireBuildingIDs());
        blockadesToLocationMap.clear();

        for(Entity entity : worldInfo.getEntitiesOfType(BLOCKADE)){
            Blockade blockade = (Blockade)entity;
            blockadesToLocationMap.put(blockade.getID(), blockade.getPosition());
        }

        updateStuckCivilians(worldInfo);

        if (options.updateUsingCommunications) {
            updateDataFromCommunication(worldInfo, messageManager);
        }

        if (options.printWorldInfo) {
            printWorldInfo(worldInfo);
        }

        if (options.incrementTimeOnUpdate) {
            currentTime += 1.0;
        }
    }

    // endregion
    // ================================================================

    // ================================================================
    // region<Update from RR WorldInfo Helpers>

    private void updateDataFromWorldInfoOld(WorldInfo worldInfo) {
        //agentTypeMap.clear();
        buildingsBurning.clear();
        civiliansOnAmbulanceMap.clear();
        blockadesToApexesMap.clear();
        humansBuried.clear();
        humansDamaged.clear();
        agentsStuck.clear();

        Collection<StandardEntity> entities = worldInfo.getAllEntities();
        for (StandardEntity entity : entities) {
            EntityID entityID = entity.getID();

            StandardEntityURN entityURN = entity.getStandardURN();
            entityIDTypeMap.put(entityID, entityURN);

            entityLocationMap.put(entityID, worldInfo.getLocation(entity));
            if (entity instanceof Human) {
                Human human = (Human) entity;
                if (human.isHPDefined() && human.getHP() > 0) {
                    EntityID positionID = human.getPosition();
                    entityPositions.put(entityID, positionID);
                    if ((entity instanceof PoliceForce || entity instanceof AmbulanceTeam || entity instanceof FireBrigade)
                            && NRLTacticsUtilities.isHumanStuck(worldInfo, human)) {
                        agentsStuck.add(entityID);
                    }
                    if (entity instanceof FireBrigade) {
                        FireBrigade fireMen = (FireBrigade) human;
                        brigadeAvailableWaterMap.put(entityID, fireMen.getWater());
                    } else if (entity instanceof Civilian) {
                        //noinspection ConstantConditions
                        if (worldInfo.getEntity(positionID).getStandardURN() == AMBULANCE_TEAM) {
                            civiliansOnAmbulanceMap.put(positionID, entityID);
                        }
                        if (NRLTacticsUtilities.isHumanStuck(worldInfo, human)) {
                            civiliansStuck.add(entityID);
                        }
                    }

                }
                if (human.isDamageDefined() && human.getDamage() > 0) {
                    humansDamaged.put(entityID, human.getDamage());
                }
                if (human.isBuriednessDefined() && human.getBuriedness() > 0) {
                    humansBuried.put(entityID, human.getBuriedness());
                }

            } else if (entity instanceof Blockade) {
                Blockade blockade = (Blockade) entity;
                entityPositions.put(entityID, blockade.getPosition());
                blockadesToApexesMap.put(entityID, toList(blockade.getApexes().clone()));
            }
        }
        updateStuckAgents(worldInfo);
        updateStuckCivilians(worldInfo);
    }

    private void updateStuckAgents(WorldInfo worldInfo) {
        Collection<StandardEntity> nonPoliceAgentIDs = worldInfo.getEntitiesOfType(StandardEntityURN.AMBULANCE_TEAM, StandardEntityURN.FIRE_BRIGADE);
        for(StandardEntity entity : nonPoliceAgentIDs) {
            Human human = (Human)entity;
            if(NRLTacticsUtilities.isHumanStuck(worldInfo, human)) {
                stuckAgentsWithPositions.put(human.getID(), human.getPosition());
            }
            else {
                stuckAgentsWithPositions.remove(human.getID());
            }
        }
    }

    private void updateStuckCivilians(WorldInfo worldInfo) {
        Collection<StandardEntity> nonPoliceAgentIDs = worldInfo.getEntitiesOfType(StandardEntityURN.CIVILIAN);
        for(StandardEntity entity : nonPoliceAgentIDs) {
            Human human = (Human)entity;
            if(human.isXDefined() && human.isYDefined()) {
                if(NRLTacticsUtilities.isHumanStuck(worldInfo, human)) {
                    stuckCiviliansWithPositions.put(human.getID(), human.getPosition());
                } else {
                    stuckCiviliansWithPositions.remove(human.getID());
                }
            }
        }
    }

    private void updateCiviliansFromWorldInfo(WorldInfo worldInfo, CommunicationMessage message) {
        EntityID senderPosition = NRLTacticsUtilities.getSenderPosition(message);
        if (senderPosition != null) {
            //noinspection ConstantConditions
            StandardEntityURN senderPositionType = worldInfo.getEntity(senderPosition).getStandardURN();
            if (senderPositionType == BUILDING) {
                exploredBuildings.add(senderPosition);
                if (!worldInfo.getBuriedHumans(senderPosition).isEmpty()) {
                    buildingsWithBuriedPeople.add(senderPosition);
                } else {
                    buildingsWithBuriedPeople.remove(senderPosition);
                }
                for (StandardEntity entity : worldInfo.getEntitiesOfType(CIVILIAN)) {
                    Civilian civilian = (Civilian) entity;
                    if (civilian.getPosition().equals(senderPosition)) {
                        if (civilian.isBuriednessDefined()
                                && civilian.getBuriedness() == 0
                                && civilian.isHPDefined()
                                && civilian.getDamage() != 0
                                && civilian.getHP() > 0) {
                            injuredCiviliansNotInRefugeWithPosition.put(civilian.getID(), senderPosition);
                        }
                    }
                    if (civilian.isHPDefined() && civilian.getHP() == 0) {
                        injuredCiviliansNotInRefugeWithPosition.remove(civilian.getID());
                    }
                }
            }
        }
    }

    private void printWorldInfo(WorldInfo worldInfo) {
        ChangeSet changeSet = worldInfo.getChanged();
        for (EntityID changedEntityID : changeSet.getChangedEntities()) {
            Set<Property> changedSet = changeSet.getChangedProperties(changedEntityID);
            List<Property> changedList = new ArrayList<>(changedSet);
            Comparator<Property> comparator = Comparator.comparing(Property::getURN);
            changedList.sort(comparator);
            StringBuilder propertyString = new StringBuilder("[");
            String sep = "";
            for (Property changed : changedSet) {
                String urn = changed.getURN().replace("urn:rescuecore2.standard:property:", "");
                int urnEnd = Math.min(urn.length(), 4);
                propertyString.append(sep).append(urn, 0, urnEnd).append(":").append(changed.getValue());
                sep = " ";
            }
            propertyString.append("]");
            logger.debug("{}: For entity:{} changes: {}", parentName, changedEntityID, propertyString.toString());
        }
    }

    private List<Integer> toList(int[] array) {
        List<Integer> list = new ArrayList<>();
        for (int value : array) {
            list.add(value);
        }
        return list;
    }

    // endregion
    // ================================================================

    // ================================================================
    // region<Update From Messages>

    private void updateDataFromCommunication(WorldInfo worldInfo, MessageManager messageManager) {
        List<CommunicationMessage> messages = messageManager.getReceivedMessageList();
        if (logger.isDebugEnabled()) {
            printMessages(messages);
        }

        for(CommunicationMessage message : messages) {
            if (options.parseRoadsAndBuildings) {
                parseRoadsAndBuildingMessages(worldInfo, message);
            }
            if (options.parseCivilianMessages) {
                parseCivilianMessages(worldInfo, message);
            }
            if (options.parseAmbulanceMessages) {
                parseAmbulanceMessages(worldInfo, message);
            }
            if (options.parseFireBrigadeMessages) {
                parseFireBrigadeMessages(worldInfo, message);
            }
            if (options.parsePoliceMessages) {
                parsePoliceMessages(worldInfo, message);
            }
        }
    }

    private void printMessages(List<CommunicationMessage> messages) {
        if (messages.size() > 0
                && options.printMessagesSeen) {
            logger.debug("{}: Messages seen:", parentName);
            for (CommunicationMessage message : messages) {
                logger.debug("{}  {}", parentName, NRLMessageUtilities.toString(message));
            }
        }
    }

    private void parseRoadsAndBuildingMessages(WorldInfo worldInfo, CommunicationMessage message) {
        EntityID senderPosition = NRLTacticsUtilities.getSenderPosition(message);
        if (senderPosition != null) {
            //noinspection ConstantConditions
            if (worldInfo.getEntity(senderPosition).getStandardURN() == ROAD) {
                exploredRoads.add(senderPosition);
            } else //noinspection ConstantConditions
                if (worldInfo.getEntity(senderPosition).getStandardURN() == BUILDING) {
                exploredBuildings.add(senderPosition);
            }
        }
	}

	private void parseCivilianMessages(WorldInfo worldInfo, CommunicationMessage message) {
        if (message.getClass() == MessageCivilian.class) {
            MessageCivilian msg = (MessageCivilian) message;
            updateStatus(worldInfo, msg.getAgentID(), CIVILIAN,
                    msg.getPosition(), msg.getBuriedness(), msg.getDamage());
        }
    }

	private void parseAmbulanceMessages(WorldInfo worldInfo, CommunicationMessage message) {
        if (message.getClass() == MessageAmbulanceTeam.class) {
            MessageAmbulanceTeam msg = (MessageAmbulanceTeam) message;
            updateStatus(worldInfo, msg.getAgentID(), AMBULANCE_TEAM,
                    msg.getPosition(), msg.getBuriedness(), msg.getDamage());

            EntityID targetID = msg.getTargetID();
            if (msg.getAction() == MessageAmbulanceTeam.ACTION_LOAD) {
                injuredCiviliansNotInRefugeWithPosition.remove(targetID);
            }

            EntityID position = msg.getPosition();
            if (msg.getAction() == MessageAmbulanceTeam.ACTION_RESCUE) {
                buildingsWithBuriedPeople.add(position);
                injuredCiviliansNotInRefugeWithPosition.put(targetID, position);
            }
            boolean isMoveAction = (msg.getAction() == MessageAmbulanceTeam.ACTION_MOVE);
            updateStuckAgentFromMessage(msg.getAgentID(), msg.getPosition(), isMoveAction);
        }
    }

    private void parseFireBrigadeMessages(WorldInfo worldInfo, CommunicationMessage message) {
        if (message.getClass() == MessageFireBrigade.class) {
            MessageFireBrigade msg = (MessageFireBrigade) message;
            updateStatus(worldInfo, msg.getAgentID(), FIRE_BRIGADE,
                    msg.getPosition(), msg.getBuriedness(), msg.getDamage());
            EntityID targetID = msg.getTargetID();
            if (msg.getAction() == MessageFireBrigade.ACTION_EXTINGUISH) {
                buildingsBurning.add(targetID);
            }
            boolean isMoveAction = (msg.getAction() == MessageFireBrigade.ACTION_MOVE);
            updateStuckAgentFromMessage(msg.getAgentID(), msg.getPosition(), isMoveAction);
        }
	}

    private void parsePoliceMessages(WorldInfo worldInfo, CommunicationMessage message) {
        if (message.getClass() == MessagePoliceForce.class) {
            MessagePoliceForce msg = (MessagePoliceForce) message;
            updateStatus(worldInfo, msg.getAgentID(), POLICE_FORCE,
                    msg.getPosition(), msg.getBuriedness(), msg.getDamage());
        }
	}

    private void updateStatus(WorldInfo worldInfo, EntityID entityID, StandardEntityURN urn, EntityID position, int buriedness, int damage) {
        if (! entityIDTypeMap.containsKey(entityID)) {
            entityIDTypeMap.put(entityID, urn);
        }
        humans.add(entityID);

        if (position != null) {
            this.entityPositions.put(entityID, position);

            if (urn == FIRE_BRIGADE
                    || urn == AMBULANCE_TEAM
                    || urn == POLICE_FORCE) {
                //noinspection ConstantConditions
                StandardEntityURN positionType = worldInfo.getEntity(position).getStandardURN();
                if (positionType == ROAD) {
                    exploredRoads.add(position);
                }
                if (positionType == BUILDING) {
                    this.entityPositions.put(entityID, position);
                    exploredBuildings.add(position);
                }
            }
        }
        if (buriedness > 0) {
            humansBuried.put(entityID, buriedness);
        } else {
            humansBuried.remove(entityID);
        }
        if (damage > 0) {
            humansDamaged.put(entityID, damage);
        } else {
            humansDamaged.remove(entityID);
        }

    }

    private void updateStuckAgentFromMessage(EntityID id, EntityID position, boolean isMoveAction){
        boolean isStuck = false;
        if(!agentsWithLastPosition.containsKey(id)){
            agentsWithLastPosition.put(id, position);
        } else {
            Set<EntityID> blockadeLocations = new HashSet<>();
            for(Map.Entry<EntityID, EntityID> entry : blockadesToLocationMap.entrySet()){
                if(blockadesWithRepairCost.get(entry.getKey()) > 0){
                    blockadeLocations.add(entry.getValue());
                }
            }
            if(isMoveAction
                    && blockadeLocations.contains(position)
                    && agentsWithLastPosition.get(id).equals(position)){
                stuckAgentsWithPositions.put(id, position);
                isStuck = true;
            } else{
                stuckAgentsWithPositions.remove(id);
            }
        }
        logger.debug("{} agent id:{} pos:{} moveAction:{} stuck:{}", parentName, id, position, isMoveAction, isStuck);
    }

    // endregion
    // ================================================================

    // ================================================================
    // region<Update WorkingMemory>

    void updateMemoryFromCurrentState() throws IllegalArgumentException {
        if (dispatcher == null) {
            String msg = "You cannot call update for a null dispatcher!";
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
        dispatcher.memory.setSimulationTime(currentTime);
        updateAgentMap();
        updateRoads();
        updateBuildings();
        updateControllableMovingAgents();
        updateCivilians();
    }

    // endregion
    // ================================================================

    // ================================================================
    // region<Update WorkingMemory Helpers>
    private void updateAgentMap() {
        for (Map.Entry<EntityID, StandardEntityURN> entityType : agentTypeMap.entrySet()) {
            EntityID entityID = entityType.getKey();
            StandardEntityURN type = entityType.getValue();
            switch (type) {
                case AMBULANCE_TEAM:
                    updateAgentState(RRPlanningDomain.AMBULANCE.instance(), entityID);
                    break;
                case FIRE_BRIGADE:
                    updateAgentState(RRPlanningDomain.BRIGADE.instance(), entityID);
                    break;
                case POLICE_FORCE:
                    updateAgentState(RRPlanningDomain.POLICE.instance(), entityID);
                    break;
                default:
                    CentralizedDispatcher.logger.warn("{}: an agent of type {} has not been added to memory", dispatcher.getShortName(), type);
            }
        }
    }

    private void updateRoads() {
        RRWorldObject tmpBinding = RRPlanningDomain.ROAD.instance();
        PredicateStatement tmpExplored = RRPlanningDomain.EXPLORED_ROAD.copier().build(tmpBinding).statement();
        dispatcher.memory.traceVariables(logger, s -> s.getTarget().nameEquals(RRPlanningDomain.EXPLORED_ROAD));
        for (EntityID id : roads) {
            tmpBinding.substitute(id);
            tmpBinding.location = id;
            updateBlocked(tmpBinding);
            updateMapAndMemory(tmpBinding);
            if (exploredRoads.contains(id)) {
                dispatcher.memory.updateOrCloneThenAdd(tmpExplored, currentTime);
            }
        }
        dispatcher.memory.traceVariables(logger, s -> s.getTarget().nameEquals(RRPlanningDomain.EXPLORED_ROAD));
        logger.debug("Updated roads");
    }

    private void updateBuildings() {
        RRWorldObject tmpBinding = RRPlanningDomain.BUILDING.instance();
        StatePredicate tmpExplored = RRPlanningDomain.EXPLORED_BUILDING.copier().build(tmpBinding);
        dispatcher.memory.traceVariables(logger, s -> s.getTarget().nameEquals(RRPlanningDomain.EXPLORED_BUILDING));
        for (EntityID id : buildings) {
            tmpBinding.substitute(id);
            tmpBinding.location = id;
            updateMapAndMemory(tmpBinding);
            updateFire(tmpBinding);
            if (exploredBuildings.contains(id)) {
                Statement statement = tmpExplored.statement();
                dispatcher.memory.updateOrCloneThenAdd(statement, currentTime);
            }
        }
        dispatcher.memory.traceVariables(logger, s -> s.getTarget().nameEquals(RRPlanningDomain.EXPLORED_BUILDING));
        logger.debug("Updated buildings");
    }

    private void updateControllableMovingAgents() {
        entityIDTypeMap.forEach((entityID, entityURN) -> {
            if (entityURN == null) {
                CentralizedDispatcher.logger.error("{}: entityURN was null for entityID {} - was an invalid entry added to the entityIDTypeMap?", dispatcher.getShortName(), entityID);
            } else if (entityURN.equals(AMBULANCE_TEAM)) {
                RRHumanObject tmpAmbulance = RRPlanningDomain.AMBULANCE.instance();
                updateAgentState(tmpAmbulance, entityID);
            } else if (entityURN.equals(FIRE_BRIGADE)) {
                RRHumanObject tmpFire = RRPlanningDomain.BRIGADE.instance();
                updateAgentState(tmpFire, entityID);
            } else if (entityURN.equals(POLICE_FORCE)) {
                RRHumanObject tmpPolice = RRPlanningDomain.POLICE.instance();
                updateAgentState(tmpPolice, entityID);
            }
        });
        CentralizedDispatcher.logger.debug("Updated agents");
    }

    private void updateCivilians() {
        entityIDTypeMap.forEach((entityID, entityURN) -> {
            if (entityURN.equals(RRPlanningDomain.CIVILIAN.getUrn())) {
                updateCivilianState(RRPlanningDomain.CIVILIAN.instance(), entityID);
            }
        });
        logger.debug("Updated civilians");
    }

    private void updateAgentState(RRHumanObject agent, EntityID entityID) {
        agent.substitute(entityID);
        agent.location = entityPositions.get(entityID);
        setBuriedAndDamageLevels(agent, entityID);
        updateMapAndMemory(agent);

        updateExplored(agent);
        updateBuried(agent);
    }

    private void updateCivilianState(RRHumanObject civilian, EntityID entityID) {
        civilian.substitute(entityID);
        civilian.location = entityPositions.get(entityID);
        setBuriedAndDamageLevels(civilian, entityID);
        updateMapAndMemory(civilian);

        updateBuried(civilian);
        updateTransport(civilian);
    }

    @SuppressWarnings("Java8MapApi")
    private void setBuriedAndDamageLevels(RRHumanObject entity, EntityID entityID) {
        if (humansBuried.containsKey(entityID)) {
            entity.setBuriedness(humansBuried.get(entityID));
        } else {
            entity.setBuriedness(0);
        }
        if (humansDamaged.containsKey(entityID)) {
            entity.setDamage(humansDamaged.get(entityID));
        } else {
            entity.setDamage(0);
        }
    }

    private void updateExplored(RRWorldObject agent) {
        EntityID location = agent.location;
        if (entityIDWorldObjectMap.containsKey(location)) {
            RRWorldObject locationObject = entityIDWorldObjectMap.get(location);
            if (locationObject.getUrn() == ROAD) {
                StatePredicate explored = RRPlanningDomain.EXPLORED_ROAD.copier().build(locationObject);
                dispatcher.memory.updateOrCloneThenAdd(explored.statement(), currentTime);
            } else {
                StatePredicate explored = RRPlanningDomain.EXPLORED_BUILDING.copier().build(locationObject);
                dispatcher.memory.updateOrCloneThenAdd(explored.statement(), currentTime);
            }
        }
    }

    private void updateBuried(RRHumanObject entity) {
        StatePredicate buried = RRPlanningDomain.BURIED.copier().build(entity);
        boolean agentIsBuried = humansBuried.containsKey(entity.getEntityId());
        PredicateStatement statement = buried.statement();

        if (dispatcher.memory.contains(statement)) {
            if (! agentIsBuried) {
                dispatcher.memory.remove(statement, currentTime);
            }
        } else {
            if (agentIsBuried) {
                dispatcher.memory.updateOrCloneThenAdd(statement, currentTime);
                if (dispatcher.agentGoals.containsKey(entity.getEntityId())) {
                    logger.debug("Previously available agent {} has been buried, unassigning its task...", entity);
                    GoalLifecycleNode goal = dispatcher.agentGoals.get(entity.getEntityId());
                    dispatcher.unassign((RRGoalBase) goal);
                }
            }
        }
    }

    private void updateTransport(RRHumanObject civilian) {
        EntityID location = civilian.location;
        if (entityIDTypeMap.containsKey(location)) {
            StandardEntityURN locationType = entityIDTypeMap.get(location);
            StatePredicate needsTransportPredicate = RRPlanningDomain.NEEDS_RESCUE.copier().build(civilian);
            PredicateStatement statement = needsTransportPredicate.statement();
            switch (locationType) {
                case REFUGE:
                    if (dispatcher.memory.contains(statement)) {
                        dispatcher.memory.remove(statement, currentTime);
                    }
                    break;
                case AMBULANCE_TEAM:
                    if (entityIDWorldObjectMap.containsKey(location)) {
                        RRWorldObject ambulance = entityIDWorldObjectMap.get(location);
                        StatePredicate transportingPredicate = RRPlanningDomain.TRANSPORTING.copier().build(ambulance, civilian);
                        dispatcher.memory.updateOrCloneThenAdd(transportingPredicate.statement(), currentTime);
                    }
                    break;
                default:
                    if (civilian.getDamage() > 0
                            || civilian.getBuriedness() > 0) {
                        dispatcher.memory.updateOrCloneThenAdd(statement, currentTime);
                    }
            }
        }
    }

    private void updateMapAndMemory(RRWorldObject worldObject) {
        RRWorldObject copy = (RRWorldObject) dispatcher.memory.updateOrCloneThenAdd(worldObject, currentTime);
        if (! entityIDWorldObjectMap.containsKey(copy.getEntityId())) {
            entityIDWorldObjectMap.put(copy.getEntityId(), copy);
        }
    }

    private void updateFire(RRWorldObject building){
        EntityID location = building.location;

        if(entityIDTypeMap.containsKey(location)) {
            boolean isOnFire = buildingsBurning.contains(location);
            StatePredicate predicate = RRPlanningDomain.ON_FIRE.copier().build(building);
            PredicateStatement statement = predicate.statement();
            if(isOnFire){
                dispatcher.memory.updateOrCloneThenAdd(statement, currentTime);
            } else if(dispatcher.memory.contains(statement)){
                dispatcher.memory.remove(statement, currentTime);
            }
        }
    }

    private void updateBlocked(RRWorldObject road){
        EntityID location = road.location;

        if(entityIDTypeMap.containsKey(location)){
            boolean containsStuckAgent = stuckAgentsWithPositions.containsValue(location);
            StatePredicate stuckAgentPredicate = RRPlanningDomain.BLOCKED_WITH_AGENT.copier().build(road);
            PredicateStatement stuckAgentStatement = stuckAgentPredicate.statement();
            if(containsStuckAgent){
                dispatcher.memory.updateOrCloneThenAdd(stuckAgentStatement, currentTime);
            } else if(dispatcher.memory.contains(stuckAgentStatement)){
                dispatcher.memory.remove(stuckAgentStatement, currentTime);
            }

            boolean containsStuckCivilian = stuckCiviliansWithPositions.containsValue(location);
            StatePredicate stuckCivilianPredicate = RRPlanningDomain.BLOCKED_WITH_CIVILIAN.copier().build(road);
            PredicateStatement stuckCivilianStatement = stuckCivilianPredicate.statement();
            if(containsStuckCivilian){
                dispatcher.memory.updateOrCloneThenAdd(stuckCivilianStatement, currentTime);
            } else if(dispatcher.memory.contains(stuckCivilianStatement)){
                dispatcher.memory.remove(stuckCivilianStatement, currentTime);
            }

            boolean containsBlockade = blockadesToLocationMap.containsValue(location);
            StatePredicate blockedRoadPredicate = RRPlanningDomain.CONTAINS_BLOCKADE.copier().build(road);
            PredicateStatement blockedRoadStatement = blockedRoadPredicate.statement();
            if(containsBlockade){
                dispatcher.memory.updateOrCloneThenAdd(blockedRoadStatement, currentTime);
            } else if(dispatcher.memory.contains(blockedRoadStatement)){
                dispatcher.memory.remove(blockedRoadStatement, currentTime);
            }
        }
    }

    // endregion
    // ================================================================

}


// ================================================================
// region<Options>

/**
 * An options closs to control agent updates.
 */
class NRLAgentStateOptions {
    public boolean incrementTimeOnUpdate = true;
    boolean initializeRoadsAndBuildingsAndAgents = true;


    boolean updateUsingCommunications = false;
    boolean parseCivilianMessages = false;
    boolean parseRoadsAndBuildings = false;
    boolean parsePoliceMessages = false;
    boolean parseAmbulanceMessages = false;
    boolean parseFireBrigadeMessages = false;

    boolean printWorldInfo = false;
    boolean printMessagesSeen = false;

    public NRLAgentStateOptions enableUpdateUsingCommunications() {
        updateUsingCommunications = true;
        parseCivilianMessages = true;
        parseRoadsAndBuildings = true;
        parsePoliceMessages = true;
        parseAmbulanceMessages = true;
        parseFireBrigadeMessages = true;
        return this;
    }

    public NRLAgentStateOptions enablePrintMessagesSeen() {
        this.printMessagesSeen = true;
        return this;
    }
}

// endregion
// ================================================================

