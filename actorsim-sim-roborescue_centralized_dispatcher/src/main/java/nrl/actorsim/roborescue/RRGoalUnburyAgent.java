package nrl.actorsim.roborescue;

import adf.agent.communication.standard.bundle.centralized.CommandAmbulance;
import nrl.actorsim.domain.StateVariable;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldType;

import java.util.Collections;

public class RRGoalUnburyAgent extends RRGoalBase {
    RRGoalUnburyAgent() {
        super(CommandAmbulance.ACTION_RESCUE,
                RRPlanningDomain.BURIED,
                RRGoalBase.CompletionCondition.MISSING_FROM_MEMORY,
                Collections.singletonList(RRPlanningDomain.AMBULANCE.getUrn()));
    }

    private RRGoalUnburyAgent(RRGoalUnburyAgent template, RRHumanObject entity) {
        super(template, entity);
    }

    @Override
    public RRGoalBase instance(WorldObject obj) {
        if (obj instanceof RRWorldObject) {
            if (isAgent(obj)) {
                return new RRGoalUnburyAgent(this, (RRHumanObject) obj);
            }
        }
        return NULL_RR_BASE_GOAL;
    }

    @Override
    public boolean matchesVariable(StateVariable sv) {
        return super.matchesVariable(sv)
                && sv.hasBindings()
                && isAgent(sv.getBinding(0));
    }

    private boolean isAgent(WorldType other) {
        return other.equalsOrInheritsFromType(RRPlanningDomain.AGENT);
    }
}
