package nrl.actorsim.roborescue;

import nrl.actorsim.domain.*;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.goalrefinement.goals.PredicateGoalBase;
import nrl.actorsim.goalrefinement.strategies.Interuptable;
import nrl.actorsim.memory.*;
import nrl.actorsim.goalrefinement.strategies.GoalMode;
import nrl.actorsim.domain.StatePredicate;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

import java.util.*;

import static nrl.actorsim.roborescue.RRWorldObject.NULL_RR_WORLD_OBJECT;

public class RRGoalBase extends PredicateGoalBase implements RRStatusUpdatable, Interuptable {
    public static RRGoalBase NULL_RR_BASE_GOAL = new RRGoalBase();

    protected int agentAction = -1;
    protected final List<StandardEntityURN> assignableAgentTypes;

    private RRWorldObject closestAgent = NULL_RR_WORLD_OBJECT;
    private List<EntityID> path;
    private double distance;

    private RRWorldObject suggestedAssignment = NULL_RR_WORLD_OBJECT;
    private RRWorldObject assignedByDispatcher = NULL_RR_WORLD_OBJECT;
    protected RRWorldObject reassignTo = NULL_RR_WORLD_OBJECT;

    List<GoalLifecycleNode> successors;

    private RRGoalBase() {
        super(PredicateStatement.ALWAYS_TRUE_STATEMENT, CompletionCondition.PRESENT_IN_MEMORY);

        assignableAgentTypes = Collections.emptyList();
    }

    /**
     * Only subclasses can instantiate this base class.
     */
    protected RRGoalBase(RRWorldObject entity,
                         int agentAction,
                         StatePredicate predicate,
                         CompletionCondition completeWhen,
                         List<StandardEntityURN> assignableAgentTypes) {
        super(predicate, completeWhen, entity);
        this.agentAction = agentAction;
        this.assignableAgentTypes = new ArrayList<>(assignableAgentTypes);
    }

    protected RRGoalBase(int agentAction,
                         StatePredicate predicate,
                         CompletionCondition completeWhen,
                         List<StandardEntityURN> assignableAgentTypes) {
        super(predicate, completeWhen);
        this.agentAction = agentAction;
        this.assignableAgentTypes = new ArrayList<>(assignableAgentTypes);
    }

    public RRGoalBase(RRGoalBase template, RRWorldObject obj) {
        this(obj,
                template.agentAction,
                template.predicate.copier().build(obj),
                template.completeWhen,
                template.assignableAgentTypes);
    }

    protected StatePredicate validatePredicate(StatePredicate predicate) {
        if (predicate == null) {
            logger.warn("Setting up a goal with a null predicate for {}", this);
        }
        return predicate;
    }

    @Override
    public RRGoalBase instance(WorldObject worldObject) {
        return NULL_RR_BASE_GOAL;
    }

    public List<StandardEntityURN> getAssignableAgentTypes() {
        return assignableAgentTypes;
    }

    @Override
    public String toString() {
        String assignString = ", unassigned";
        if (isAssignedByDispatcher()) {
            assignString = ", assigned:" + getAssignedByDispatcher().toString();
        }
        return super.toString() + assignString;
    }

    void updateClosestAgent() {
        CentralizedDispatcher dispatcher = CentralizedDispatcher.getInstance();
        NRLTacticsUtilities.ClosestResult result = dispatcher.getClosestAvailableScoutingAgent(getEntity());
        closestAgent = result.closestAgent;
        if (closestAgent != NULL_RR_WORLD_OBJECT) {
            setPath(result.shortestPath);
            setDistance(result.distance);
        } else {
            setPath(null);
        }
    }

    public boolean hasInterruptableSuccessors(WorkingMemory memory) {
        CentralizedDispatcher dispatcher = CentralizedDispatcher.getInstance();
        return (remainsUnassignedByDispatcher())
                && (getAvailableAgents().size() == 0)
                && (getAssignedInterruptableSuccessors(memory).size() > 0);
    }

    public void assign(RRWorldObject agent) {
        setAssignedByDispatcher(agent);
    }

    public void unassign() {
        setAssignedByDispatcher(NULL_RR_WORLD_OBJECT);
        this.evaluate();
    }

    @Override
    public void updateStatus() {
        if (assignedByDispatcher == NULL_RR_WORLD_OBJECT) {
            statusString = "";
        } else {
            String buriedString = "";
            String damagedString = "";
            WorkingMemory workingMemory = CentralizedDispatcher.getInstance().getWorkingMemory();

            if (entity instanceof RRHumanObject
                    && workingMemory.contains(entity)) {
                RRHumanObject human = (RRHumanObject) workingMemory.getObject(entity);
                buriedString = " b:" + human.getBuriedness();
                damagedString = " d:" + human.getDamage();
            }
            statusString = String.format("%s%s%s", assignedByDispatcher.toString(), buriedString, damagedString);
        }

    }

    public boolean needsEvaluate() {
        return false;
    }

    // ==========================================================
    // region<Accessors>

    @Override
    public RRWorldObject getEntity() {
        return (RRWorldObject) entity;
    }

    public int getAgentAction() {
        return agentAction;
    }

    public void setAgentAction(int agentAction) {
        this.agentAction = agentAction;
    }

    public StatePredicate getPredicate() {
        return predicate;
    }

    public CompletionCondition getCompleteWhen() {
        return completeWhen;
    }

    public RRWorldObject getClosestAgent() {
        return closestAgent;
    }

    public List<EntityID> getPath() {
        return path;
    }

    public void setPath(List<EntityID> path) {
        this.path = path;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    static class CompareByDistance implements Comparator<RRGoalBase> {
        @Override
        public int compare(RRGoalBase left, RRGoalBase right) {
            return (int) (left.getDistance() - right.getDistance());
        }
    }

    public RRWorldObject getSuggestedAssignment() {
        return suggestedAssignment;
    }

    public void setSuggestedAssignment(RRWorldObject suggestedAssignment) {
        this.suggestedAssignment = suggestedAssignment;
    }

    public RRWorldObject getAssignedByDispatcher() {
        return assignedByDispatcher;
    }

    public boolean isAssignedByDispatcher() {
        return assignedByDispatcher != NULL_RR_WORLD_OBJECT;
    }

    public boolean remainsUnassignedByDispatcher() {
        return assignedByDispatcher == NULL_RR_WORLD_OBJECT;
    }

    public void setAssignedByDispatcher(RRWorldObject assignedByDispatcher) {
        this.assignedByDispatcher = assignedByDispatcher;
    }

    /**
     * Update and return the list of successors.
     *
     * @param memory the memory to check for successor goals
     * @return the list of successors
     */
    public List<GoalLifecycleNode> getSuccessors(WorkingMemory memory) {
        GoalMemorySimple goalMemory = memory.getGoalMemory();
        successors = goalMemory.getSuccessors(this, GoalMode.DISPATCHED);
        return successors;
    }

    List<RRWorldObject> getAvailableAgents() {
        return CentralizedDispatcher.getInstance().getAvailableAgents(getAssignableAgentTypes());
    }

    /**
     * Update and return the list of successors.
     *
     * @param memory the memory to check for successor goals
     * @return the list of successors
     */
    public List<GoalLifecycleNode> getAssignedInterruptableSuccessors(WorkingMemory memory) {
        GoalMemorySimple goalMemory = memory.getGoalMemory();
        successors = goalMemory.getSuccessors(this, GoalMode.DISPATCHED);
        successors.removeIf(goal -> !(goal instanceof Interuptable)  //goal isn't an Interuptable
                || !((Interuptable) goal).isCurrentlyAssigned() // goal is not currently assigned
                || (goal.getClass().equals(this.getClass())) // goal is the same type as this goal
                || (getAssignableAgentTypes().size() > 0
                && (!getAssignableAgentTypes().contains(((RRGoalBase) goal).getAssignedByDispatcher().getUrn()))));
        return successors;
    }

    /**
     * An accessor method to get the successors produced by getAssignedInterruptableSuccessors()
     *       or hasInterruptableSuccessors().  It should only be called in the same scope as the producers!
     * @return the list of successors
     */
    public List<GoalLifecycleNode> getSuccessors() {
        List<GoalLifecycleNode> result = successors;
        if (result == null) {
            result = Collections.emptyList();
        }
        return result;
    }

    @Override
    public boolean isCurrentlyAssigned() {
        return isAssignedByDispatcher();
    }

    @Override
    public void interrupt() {
        CentralizedDispatcher dispatcher = CentralizedDispatcher.getInstance();
        dispatcher.unassign(this);
    }

    public boolean hasBeenReassigned() {
        return reassignTo != NULL_RR_WORLD_OBJECT;
    }

    public RRWorldObject getReassigment() {
        return reassignTo;
    }

    public void clearReassigment() {
        reassignTo = NULL_RR_WORLD_OBJECT;
    }

    // endregion
    // ==========================================================

}
