package nrl.actorsim.roborescue;

public interface RRStatusUpdatable {
    void updateStatus();
}
