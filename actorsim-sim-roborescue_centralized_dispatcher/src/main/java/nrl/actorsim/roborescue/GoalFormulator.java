package nrl.actorsim.roborescue;

import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.GoalMemorySimple;
import org.slf4j.LoggerFactory;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

import java.util.Set;


class CannotConstructGoalException extends RuntimeException {
    CannotConstructGoalException(String message) {
        super(message);
    }
}

/**
 * A factor class to formulate goals directly rather than use
 * a strategy.
 *
 * Client code will call this method:
 *
 *       GoalFormulator adapter = GoalFormulator.getInstance();
 *       String id = "<valid id>";
 *       Goal goal = adapter.buildUnburyGoal(id);
 *       adapter.formulateGoal(goal);
 *
 */
@SuppressWarnings("unused")
public class GoalFormulator {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(GoalFormulator.class);
    private static GoalFormulator instance = null;

    public static GoalFormulator getInstance() {
        if (instance == null) {
            instance = new GoalFormulator();
        }
        return instance;
    }

    public RRGoalBase buildUnburyGoal(String id) {
        RRGoalBase newGoal = null;
        CentralizedDispatcher dispatcher = CentralizedDispatcher.getInstance();
        RRWorldObject worldObject = dispatcher.getObject(id);
        if (worldObject instanceof RRHumanObject) {
            RRHumanObject human = (RRHumanObject) worldObject;
            StandardEntityURN urn = human.getUrn();
            EntityID entityID = human.getEntityId();
            if (urn == StandardEntityURN.CIVILIAN) {
                RRGoalBase template = new RRGoalRescueCivilian();
                newGoal = template.instance(human);
            } else if (urn == StandardEntityURN.POLICE_FORCE
                    || urn == StandardEntityURN.AMBULANCE_TEAM
                    || urn == StandardEntityURN.FIRE_BRIGADE) {
                RRGoalBase template = new RRGoalUnburyAgent();
                newGoal = template.instance(human);
            }
        }
        if (newGoal == null) {
            throw new CannotConstructGoalException("Could not construct goal for id " + id);
        }
        return newGoal;
    }

    public void formulateGoal(GoalLifecycleNode goalToFormulate, GoalMemorySimple memory) {
        Set<GoalLifecycleNode> matches = memory.find(goal -> goal.getName().equals(goalToFormulate.getName()));
        if (matches.size() == 0) {
            goalToFormulate.formulate(memory);
            logger.debug("Formulated goal {}", goalToFormulate);
        }

        logger.debug("FAILED to formulate goal {} due to matchesInMemory:{}", goalToFormulate, matches.size());
    }
}
