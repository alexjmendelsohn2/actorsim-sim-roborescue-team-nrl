package exploring;

import java.util.List;
import java.util.Set;

import adf.agent.action.Action;
import adf.agent.action.ambulance.ActionRescue;
import adf.agent.action.common.ActionMove;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.MessageUtil;
import adf.agent.communication.standard.bundle.StandardMessage;
import adf.agent.communication.standard.bundle.centralized.CommandAmbulance;
import adf.agent.communication.standard.bundle.information.MessageBuilding;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.communication.CommunicationMessage;
import adf.component.extaction.ExtAction;
import adf.component.module.algorithm.PathPlanning;
import adf.component.tactics.TacticsAmbulanceTeam;
import adf.debug.WorldViewLauncher;
import adf.sample.module.algorithm.SamplePathPlanning;
import rescuecore2.standard.entities.AmbulanceTeam;
import rescuecore2.standard.entities.Building;
import rescuecore2.standard.entities.Road;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.worldmodel.EntityID;

import static adf.agent.communication.standard.bundle.centralized.CommandAmbulance.ACTION_RESCUE;
import static rescuecore2.standard.entities.StandardEntityURN.*;

public class CounterClockWiseAmbulanceTactics extends TacticsAmbulanceTeam {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CounterClockWiseAmbulanceTactics.class);

	private Boolean isVisualDebug;
	private Graph graph;
	private List<EntityID> previousPath;
	private ExtAction ambulanceAction;
	private PathPlanning pathPlanning;
	private boolean isRescuing;
	//	private MessageTool messageTool;
	@Override
	public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        logger.info("Initilialzed {} with id {}", this.getClass(), agentInfo.me());
		worldInfo.indexClass(
				CIVILIAN,
				FIRE_BRIGADE,
				POLICE_FORCE,
				AMBULANCE_TEAM,
				ROAD,
				HYDRANT,
				BUILDING,
				REFUGE,
				GAS_STATION,
				AMBULANCE_CENTRE,
				FIRE_STATION,
				POLICE_OFFICE
				);

		//		this.messageTool = new MessageTool(scenarioInfo, developData);

		this.isVisualDebug = (scenarioInfo.isDebugMode()
				&& moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));
		this.graph = new Graph(worldInfo.getRawWorld());
		this.previousPath = null;
		this.ambulanceAction = new AmbulanceExtAction(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
		this.pathPlanning = new SamplePathPlanning(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
		this.isRescuing = false;
		registerModule(this.ambulanceAction);
		registerModule(this.pathPlanning);
	}

	@Override
	public void precompute(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {
		modulesPrecompute(precomputeData);
	}

	@Override
	public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager,
			PrecomputeData precomputeData, DevelopData developData) {
		modulesResume(precomputeData);
		if (isVisualDebug){
			WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
		}
	}

	@Override
	public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, DevelopData developData) {
		modulesPreparate();
		if (isVisualDebug){
			WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
		}
	}

	@Override
	public Action think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
			ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
		logger.info("Starting think");
		this.updateWorldInfoFromMessages(agentInfo, worldInfo, messageManager);
		sendUpdates(agentInfo, worldInfo, messageManager);
		//modulesUpdateInfo(messageManager);
		pathPlanning.updateInfo(messageManager);
		
		AmbulanceTeam agent = (AmbulanceTeam)agentInfo.me();
		EntityID position = agent.getPosition();
		Action action = null;

		if(!this.isRescuing 
				&& agentInfo.someoneOnBoard() == null) {
			for(CommunicationMessage message : messageManager.getReceivedMessageList()) {
				if (message instanceof CommandAmbulance) {
					CommandAmbulance command = (CommandAmbulance) message;
					if (!command.getTargetID().equals(agentInfo.getPosition())
							&& action == null) {

						pathPlanning.setFrom(position);
						pathPlanning.setDestination(command.getTargetID());
						List<EntityID> rescuePath = pathPlanning.calc().getResult();
						action = new ActionMove(rescuePath);
					}
				}
			}
		}

		int indexOfPos = previousPath == null ? 0 : previousPath.indexOf(position);

		if((previousPath == null || indexOfPos == previousPath.size() - 1 || previousPath.size() == 0) 
				&& action == null) {
			List<EntityID> cycle = graph.findCycle(position);
			if(cycle.size() <= 1) {
				cycle.add(getFirstNeighborRoad(position, worldInfo));
			}
			previousPath = cycle;
			indexOfPos = 0;
		}
		EntityID target;
		List<EntityID> currentPath;
		if(indexOfPos == -1) {
			pathPlanning.setFrom(position);
			if(previousPath.size() != 0) {
				pathPlanning.setDestination(previousPath.get(0));
			}
			else {
				pathPlanning.setDestination(getFirstNeighborRoad(position, worldInfo));
			}
			List<EntityID> returnPath = pathPlanning.calc().getResult();
			target = returnPath.get(0);
			currentPath = returnPath;
		}
		else {
			previousPath = previousPath.subList(indexOfPos + 1, previousPath.size());
			if(previousPath.size() != 0) {
				target = previousPath.get(0);
			}
			else {
				target = this.getFirstNeighborRoad(position, worldInfo);
			}
			currentPath = previousPath;
		}

		if(action == null) {
			action = this.ambulanceAction.setTarget(target).calc().updateInfo(messageManager).getAction();
		}


		if(action == null) {
			action = new ActionMove(currentPath);
		}

		calcMessage(action, agentInfo, messageManager);
		
		return action;
	}
	
	private void updateWorldInfoFromMessages(AgentInfo agentInfo, WorldInfo worldInfo, MessageManager messageManager) {
		Set<EntityID> changedEntitiesIDs = worldInfo.getChanged().getChangedEntities();
		changedEntitiesIDs.add(agentInfo.getID());
		for(CommunicationMessage message : messageManager.getReceivedMessageList()) {
			if (message instanceof StandardMessage) {
				MessageUtil.reflectMessage(worldInfo, (StandardMessage) message);
			}
		}

	}
	
	private void sendUpdates(AgentInfo agentInfo, WorldInfo worldInfo, MessageManager messageManager) {
		Set<EntityID> changedEntitiesIDs = worldInfo.getChanged().getChangedEntities();
		for(EntityID changedEntityID : changedEntitiesIDs) {
			StandardEntity entity = worldInfo.getEntity(changedEntityID);
			if((entity.getStandardURN() == BUILDING)
                    && StaticUtilityMethods.isOnFireOrWaterDameged((Building) entity)){
				messageManager.addMessage(new MessageBuilding(true, (Building)entity));
			}
		}
	}

	private void calcMessage(Action action, AgentInfo agentInfo, MessageManager messageManager) {
		if(action instanceof ActionRescue) {
		    EntityID id = agentInfo.getID();
            EntityID position = agentInfo.getPosition();
			messageManager.addMessage(new CommandAmbulance(true, id, position, ACTION_RESCUE));
			this.isRescuing = true;
		}
		else {
			this.isRescuing = false;
		}
		
	}
	
	private EntityID getFirstNeighborRoad(EntityID currentPosition, WorldInfo worldInfo) {
		Set<EntityID> neighbors = graph.getNeighbors(currentPosition);
		for(EntityID neighbor : neighbors) {
			if(worldInfo.getEntity(neighbor) instanceof Road) {
				return neighbor;
			}
		}
		return null;
	}
	

}
