package exploring;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import adf.agent.action.Action;
import adf.agent.action.common.ActionMove;
import adf.agent.action.police.ActionClear;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.component.extaction.ExtAction;
import rescuecore2.misc.geometry.Vector2D;
import rescuecore2.standard.entities.Blockade;
import rescuecore2.standard.entities.PoliceForce;
import rescuecore2.standard.entities.Road;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.worldmodel.EntityID;

public class ClearPathExtAction extends ExtAction{

	private EntityID target;
	private int clearDistance;

	public ClearPathExtAction(AgentInfo ai, WorldInfo wi, ScenarioInfo si, ModuleManager moduleManager,
			DevelopData developData) {
		super(ai, wi, si, moduleManager, developData);
		this.target = null;
		this.clearDistance = si.getClearRepairDistance();
	}

	@Override
	public ExtAction setTarget(EntityID targets) {
		this.target = targets;
		return this;
	}

	@Override
	public ExtAction calc() {
		this.result = null;
		PoliceForce agent = (PoliceForce) agentInfo.me();
		StandardEntity targetEntity = this.worldInfo.getEntity(this.target);
		if(targetEntity instanceof Road) {
			this.result = clearRoad(agent, targetEntity);
		}

		return this;
	}

	private Action clearRoad(PoliceForce agent, StandardEntity target) {

		if(target instanceof Road) {
			Road road = (Road)target;
			if(road.isBlockadesDefined() && road.getBlockades().size() > 0) {
				Collection<Blockade> blockades = this.worldInfo.getBlockades(road)
						.stream()
						.filter(Blockade::isApexesDefined)
						.collect(Collectors.toSet());
				Blockade clearBlockade = null;

				int minIntersectionDistance = Integer.MAX_VALUE;

				for (Blockade blockade : blockades)
				{
					for (Blockade another : blockades)
					{
						if (!blockade.getID().equals(another.getID()) && this.intersect(blockade, another))
						{
							int distance1 = this.worldInfo.getDistance(agent, blockade);
							int distance2 = this.worldInfo.getDistance(agent, another);
							if (distance1 <= distance2 && distance1 < minIntersectionDistance)
							{
								minIntersectionDistance = distance1;
								clearBlockade = blockade;
							}
							else if (distance2 < minIntersectionDistance)
							{
								minIntersectionDistance = distance2;
								clearBlockade = another;
							}
						}
					}
				}

				if (clearBlockade != null){
					if (minIntersectionDistance < this.clearDistance - 2){
						return new ActionClear(clearBlockade);
					}
					else{
						return new ActionMove(
								Arrays.asList(agent.getPosition()),
								clearBlockade.getX(),
								clearBlockade.getY()
								);
					}
				}

				double minDistance = Double.MAX_VALUE;
				double agentX = agent.getX();
				double agentY = agent.getY();
				int clearX = 0;
				int clearY = 0;
				for(Blockade blockade : blockades) {
					int[] apexes = blockade.getApexes();
					for (int i = 0; i < (apexes.length - 2); i += 2){
						double distance = this.getDistance(agentX, agentY, apexes[i], apexes[i + 1]);
						if (distance < minDistance){
							clearBlockade = blockade;
							minDistance = distance;
							clearX = apexes[i];
							clearY = apexes[i + 1];
						}
					}
					
				}
				if(clearBlockade != null) {
					if(minDistance < this.clearDistance - 2) {
						Vector2D vector = new Vector2D(clearX - agentX, clearY - agentY).normalised().scale(this.clearDistance);
						clearX = (int) (agentX + vector.getX());
						clearY = (int) (agentY + vector.getY());
						return new ActionClear(clearX, clearY, clearBlockade);
					}
					return new ActionMove(Arrays.asList(agent.getPosition()), clearX, clearY);
				}

			}
		}

		return null;
	}

	private boolean intersect(Blockade blockade, Blockade another) {
		if (blockade.isApexesDefined() && another.isApexesDefined()){
			int[] apexes0 = blockade.getApexes();
			int[] apexes1 = another.getApexes();
			for (int i = 0; i < (apexes0.length - 2); i += 2){
				for (int j = 0; j < (apexes1.length - 2); j += 2){
					if (java.awt.geom.Line2D.linesIntersect(
							apexes0[i], apexes0[i + 1], apexes0[i + 2], apexes0[i + 3],
							apexes1[j], apexes1[j + 1], apexes1[j + 2], apexes1[j + 3]
							)){
						return true;
					}
				}
			}
			for (int i = 0; i < (apexes0.length - 2); i += 2){
				if (java.awt.geom.Line2D.linesIntersect(
						apexes0[i], apexes0[i + 1], apexes0[i + 2], apexes0[i + 3],
						apexes1[apexes1.length - 2], apexes1[apexes1.length - 1], apexes1[0], apexes1[1]
						)){
					return true;
				}
			}
			for (int j = 0; j < (apexes1.length - 2); j += 2){
				if (java.awt.geom.Line2D.linesIntersect(
						apexes0[apexes0.length - 2], apexes0[apexes0.length - 1], apexes0[0], apexes0[1],
						apexes1[j], apexes1[j + 1], apexes1[j + 2], apexes1[j + 3]
						)){
					return true;
				}
			}
		}
		return false;
	}

	private double getDistance(double fromX, double fromY, double toX, double toY)
	{
		double dx = toX - fromX;
		double dy = toY - fromY;
		return Math.hypot(dx, dy);
	}

}
