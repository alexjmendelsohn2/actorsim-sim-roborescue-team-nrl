package exploring;

import static rescuecore2.standard.entities.StandardEntityURN.HYDRANT;
import static rescuecore2.standard.entities.StandardEntityURN.REFUGE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import adf.agent.action.Action;
import adf.agent.action.common.ActionMove;
import adf.agent.action.fire.ActionExtinguish;
import adf.agent.action.fire.ActionRefill;
import adf.agent.communication.MessageManager;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.component.extaction.ExtAction;
import adf.component.module.algorithm.PathPlanning;
import adf.sample.module.algorithm.SamplePathPlanning;
import rescuecore2.misc.Pair;
import rescuecore2.standard.entities.Building;
import rescuecore2.standard.entities.FireBrigade;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;

public class FireFightExtAction extends ExtAction{

	private EntityID target;
	private boolean needsRefill;
	private int refillCompleted;
	private int refillRequest;
	private int maxExtinguishPower;
	private int maxExtinguishDistance;
	private PathPlanning pathPlanning;

	public FireFightExtAction(AgentInfo ai, WorldInfo wi, ScenarioInfo si, ModuleManager moduleManager,
			DevelopData developData) {
		super(ai, wi, si, moduleManager, developData);
		this.needsRefill = false;
		this.target = null;
		int maxWater = scenarioInfo.getFireTankMaximum();
		this.maxExtinguishDistance = scenarioInfo.getFireExtinguishMaxDistance();
		this.maxExtinguishPower = scenarioInfo.getFireExtinguishMaxSum();
		this.refillCompleted = (maxWater / 10) * developData.getInteger("ActionFireFighting.refill.completed", 10);
		this.refillRequest = this.maxExtinguishPower * developData.getInteger("ActionFireFighting.refill.request", 1);
		this.pathPlanning = new SamplePathPlanning(ai, wi, si, moduleManager, developData);
	}

	@Override
	public ExtAction setTarget(EntityID targets) {
		this.target = targets;
		return this;
	}

	@Override
	public ExtAction calc() {
		this.result = null;
		FireBrigade agent = (FireBrigade)this.agentInfo.me();
		this.needsRefill = this.needsRefill(agent, this.needsRefill);
		if(this.needsRefill) {
			this.result = this.calcRefill(agent, this.pathPlanning, this.target);
		}
		else {
			this.result = this.calcExtinguishAction(agent, pathPlanning, target);
		}
		if(this.result == null && this.target != null) {
			this.result = this.calcRouteToFire(agent, pathPlanning, target);
		}
		return this;
	}
	
	
	private boolean needsRefill(FireBrigade agent, boolean needsRefill) {
		if (needsRefill)
        {
            StandardEntityURN positionURN = Objects.requireNonNull(this.worldInfo.getPosition(agent)).getStandardURN();
            return !(positionURN == REFUGE || positionURN == HYDRANT) || agent.getWater() < this.refillCompleted;
        }
        return agent.getWater() <= this.refillRequest;
	}
	
	private Action calcRefill(FireBrigade agent, PathPlanning pathPlanner, EntityID target) {
		StandardEntityURN locationURN = Objects.requireNonNull(this.worldInfo.getEntity(agent.getPosition())).getStandardURN();
		if(locationURN == REFUGE) {
			return new ActionRefill();
		}
		Action action = findSupply(agent, pathPlanner, target, REFUGE);
		if(action != null) {
			return action;
		}
		action = findSupply(agent, pathPlanner, target, HYDRANT);
		
		return action;
	}
	
	private Action findSupply(FireBrigade agent, PathPlanning pathPlanning, EntityID target, StandardEntityURN urn) {
		EntityID position = agent.getPosition();
		Collection<EntityID> supplyIDs = this.worldInfo.getEntityIDsOfType(urn);
		if(!supplyIDs.isEmpty()) {
			if(supplyIDs.contains(position)) {
				return new ActionRefill();
			}
			List<EntityID> firstPath = null;
			for(EntityID supplyUnit : supplyIDs) {
				pathPlanning.setFrom(position);
				pathPlanning.setDestination(supplyUnit);
				List<EntityID> path = pathPlanning.calc().getResult();
				if(path != null) {
					if(firstPath == null) {
						firstPath = new ArrayList<>(path);
					}
					pathPlanning.setFrom(supplyUnit);
					pathPlanning.setDestination(target);
					List<EntityID> returnToTargetPath = pathPlanning.calc().getResult();
					if(returnToTargetPath != null 
							&& returnToTargetPath.size() > 0) {
						return new ActionMove(path);
					}
				}
			}
			return firstPath != null ? new ActionMove (firstPath) : null;
		}
		
		return null;
	}
	
	public ExtAction updateInfo(MessageManager messageManager) {
		this.pathPlanning.updateInfo(messageManager);
		return this;
	}
	
	private Action calcExtinguishAction(FireBrigade agent, PathPlanning pathPlanning, EntityID target) {

		Pair<Integer, Integer> agentLoc = agent.getLocation(this.worldInfo.getRawWorld());
		Collection<Building> fireBuildings = worldInfo.getFireBuildings();
		for(Building building : fireBuildings) {
			Pair<Integer, Integer> buildingLoc = building.getLocation(this.worldInfo.getRawWorld());
			if(this.getDistance(agentLoc, buildingLoc) < this.maxExtinguishDistance) {
				return new ActionExtinguish(building, this.maxExtinguishPower);
			}
		}
		return null;
	}
	
	private Action calcRouteToFire(FireBrigade agent, PathPlanning pathPlanning, EntityID target) {
		pathPlanning.setFrom(agent.getPosition());
		pathPlanning.setDestination(target);
		List<EntityID> path = pathPlanning.calc().getResult();
		if(path != null && path.size() > 0) {
			return new ActionMove(path);
		}
		return null;
	}
	
	private double getDistance(Pair<Integer, Integer> loc1, Pair<Integer, Integer> loc2) {
		return Math.hypot(loc1.first() - loc2.first(), loc1.second() - loc2.second());
	}

}
