from google.protobuf import message

from pyhop_networked_connector import PyhopNetworkedConnector
from threading import Thread
from pyhop_connector import PyhopConnector
from pyhop_extended import PyhopOptions, PyhopExecResult, State
from roborescue_operators import *
from roborescue_methods import *
import transferState_pb2

class RoboRescue_Pyhop_Network(PyhopNetworkedConnector):

    def __init__(self, addr="localhost", port=9898):
        PyhopNetworkedConnector.__init__(self, addr, port)
        self.running = True

    class ServerThread(Thread):

        def __init__(self, socket, thread_id, pyhop_connector : PyhopConnector):
            Thread.__init__(self)
            self.socket = socket
            self.thread_id = thread_id
            self.pyhop_connector = pyhop_connector

        def run(self):
            self.pyhop_connector.wait_for_thread(self.thread_id)

            results = self.pyhop_connector.thread_id_to_returned_data[self.thread_id]

            if results.success:
                sending_plan = transferState_pb2.Plan()
                for primitive in results.partial_plan:
                    plan_action = sending_plan.plan.add()
                    plan_action.action_name = primitive[0]
                    for i in range(1, len(primitive)):
                        plan_action.action_parameters.append(str(primitive[i]))


                self.socket.sendall(sending_plan.SerializeToString())

            self.socket.close()



    def __convert_protobuff_to_pyhop(self, protobuf_state : transferState_pb2.TransferState):
        state = State('current state')
        state.buried = set(protobuf_state.buried.element)
        state.injured = set(protobuf_state.injured.element)
        state.hydrants = set(protobuf_state.hydrants.element)
        state.refuges = set(protobuf_state.refuge.element)
        state.fire_buildings = set(protobuf_state.fire_buildings.element)
        state.stuck_agents = set(protobuf_state.stuck_agents.element)
        state.stuck_civilians = set(protobuf_state.stuck_civilians.element)
        state.unexplored_roads = set(protobuf_state.unexplored_roads.element)
        state.unexplored_buildings = set(protobuf_state.unexplored_buildings.element)

        state.max_water_range = protobuf_state.max_water_range
        state.max_water_power = protobuf_state.max_water_power
        state.max_clear_distance = protobuf_state.max_clear_distance

        state.closest_injured_in_set = protobuf_state.closest_injured_in_set
        state.closest_buried_in_set = protobuf_state.closest_buried_in_set
        state.closest_hydrant_in_set = protobuf_state.closest_hydrant_in_set
        state.closest_refuge_in_set = protobuf_state.closest_refuge_in_set
        state.closest_fire_building_in_set = protobuf_state.closest_fire_building_in_set
        state.closest_stuck_agent_id = protobuf_state.closest_stuck_agent_id

        state.location = dict(protobuf_state.location)
        state.position = dict(protobuf_state.position)
        state.action = dict(protobuf_state.action)
        state.type = dict(protobuf_state.type)
        state.on_board = dict(protobuf_state.on_board)
        state.water = dict(protobuf_state.water)

        state.blockades_to_apexes_map = dict()
        for key in protobuf_state.blockades_to_apexes_map:
            state.blockades_to_apexes_map[key] = list(protobuf_state.blockades_to_apexes_map[key].element)

        state.neighbors = dict()
        for key in protobuf_state.neighbors:
            state.neighbors[key] = set(protobuf_state.neighbors[key].element)

        return state


    def execute(self):

        while self.running:

            client_socket, addr = self.socket.accept()

            protobuff_state = transferState_pb2.TransferState()

            received = client_socket.recv(9990000)
            print(received)
            print(type(received))
            try:
                protobuff_state.ParseFromString(received)

                state = self.__convert_protobuff_to_pyhop(protobuff_state)
                options = PyhopOptions(state, [('act', protobuff_state.agent_id)], [], -1)

                options.update_operators(move, move_to_point, move_until_in_range, rest, rescue, load, unload,
                                                    refill, extinguish, clear, clear_current_area, clear_road)

                options.update_methods("explore", explore_buildings, explore_roads)
                options.update_methods("carry_injured", carry_injured)
                options.update_methods("save", save_buried_civilian, save_buried_agent)
                options.update_methods("refill_at_nearest", refill_at_nearest_hydrant, refill_at_nearest_refuge)
                options.update_methods("go_to_entity_id_and_clear", go_to_entity_id_and_clear)
                options.update_methods("free_nearest_agent", free_nearest_agent)
                options.update_methods("free_nearest_civilian", free_nearest_civilian)
                options.update_methods("unclear_nearest_building", unclear_nearest_building)
                options.update_methods("free_all_agents", free_all_agents)
                options.update_methods("free_all_civilians", free_all_civilians)
                options.update_methods("unclear_paths_to_all_buildings", unclear_paths_to_all_buildings)
                options.update_methods("clear_closest_blockade", clear_closest_blockade)
                options.update_methods("act", ambulance_act, fire_act, police_act)

                thread_id = self.spawn_thread(options)

                server_thread = RoboRescue_Pyhop_Network.ServerThread(client_socket, thread_id, self)

                server_thread.run()
            except message.DecodeError as e:
                print("Parsing failed for object '{}' with error '{}'".format(received, e))


network = RoboRescue_Pyhop_Network()

network.setup_network()

network.execute()