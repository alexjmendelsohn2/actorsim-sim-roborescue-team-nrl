import copy
import math


class Pair:

    def __init__(self, first, second):
        self.x = first
        self.y = second

    def __repr__(self):
        return "pair({}, {})".format(self.x, self.y)


def get_distance(from_x, from_y, to_x, to_y):
    dx = math.fabs(to_x - from_x)
    dy = math.fabs(to_y - from_y)
    return math.hypot(dx, dy)


def get_distance_between_locations(location_1, location_2):
    return get_distance(location_1.x, location_1.y, location_2.x, location_2.y)


def get_distance_between_point_and_line_segment(point, point1_on_line, point2_on_line):
    segment_length = get_distance_between_locations(point1_on_line, point2_on_line)

    twice_triangle_area = math.fabs((point2_on_line.y-point1_on_line.y) * point.x
                                    - (point2_on_line.x - point1_on_line.x) * point.y
                                    + point2_on_line.x * point1_on_line.y
                                    - point2_on_line.y * point1_on_line.x)
    return twice_triangle_area / segment_length


def get_closest_from_set(state, agent_id, set):
    min_distance = 0
    closest_element_id = -1
    agent_location = state.location[agent_id]
    for entity_id in set:
        distance = get_distance_between_locations(agent_location, state.location[entity_id])
        if closest_element_id == -1 or distance < min_distance:
            min_distance = distance
            closest_element_id = entity_id

    return closest_element_id


def get_closest_from_set_of_blockades(state, agent_id, set_of_blockades):
    min_distance = 0
    closest_element_id = -1
    closest_line = None
    agent_location = state.location[agent_id]
    for entity_id in set_of_blockades:
        for i in range (0, len(set_of_blockades[entity_id]) - 1, 2):
            line = (set_of_blockades[entity_id][i], set_of_blockades[entity_id][i + 1])
            distance = get_distance_between_point_and_line_segment(agent_location, line[0], line[1])
            if closest_element_id == -1 or distance < min_distance:
                min_distance = distance
                closest_element_id = entity_id
                closest_line = line

    return closest_element_id, closest_line


def find_a_perpendicular_given_a_point_and_a_line(point_location, line_point1, line_point2):

    k = ((line_point2.y - line_point1.y) * (point_location.x - line_point1.x)
         - (line_point2.x - line_point1.x) * (point_location.y - line_point1.y)) \
        / ((line_point2.y - line_point1.y) ** 2 + (line_point2.x - line_point1.x) ** 2)

    x = point_location.x - k * (line_point2.y - line_point1.y)
    y = point_location.y + k * (line_point2.x - line_point1.x)

    return Pair(x, y)


def get_blockade_clear_point_in_range(state, agent_id, blockade_id):
    apexes = state.blockades_to_apexes_map[blockade_id]
    min_distance = state.max_clear_distance
    x = None
    y = None
    for i in range(len(apexes) - 2):
        distance = get_distance(state[agent_id].x, state[agent_id].y, apexes[i], apexes[i + 1])
        if distance < min_distance:
            x = apexes[i]
            y = apexes[i + 1]
            min_distance = distance
    return x, y


def find_clear_roads_near_buildings(state):
    buildings = set()
    blockades = set()

    for entity_id in state.type:
        if state.type == "BUILDING":
            buildings.add(entity_id)
        elif state.type == "BLOCKADE":
            blockades.add(entity_id)

    roads_to_clear = set()

    for building in buildings:
        for blockade in blockades:
            if state.position[blockade] in state.neighbors[building]:
                roads_to_clear.add(state.position[blockade])

    return roads_to_clear


##################################################
#
#    Methods available to all agents
#
##################################################


def explore_buildings(state, agent_id):
    if state.unexplored_buildings:
        return [('move', agent_id, get_closest_from_set(state, agent_id, state.unexplored_buildings))]
    else:
        return False


def explore_roads(state, agent_id):
    if not state.unexplored_roads:
        state.unexplored_roads = copy.deepcopy(state.roads)
    return [('move', agent_id, get_closest_from_set(state, agent_id, state.unexplored_roads))]


##################################################
#
#    Methods available to the ambulance team
#
##################################################


def carry_injured(state, agent_id, target_id):
    return [('load', agent_id, target_id),
            ('move', agent_id, get_closest_from_set(state, agent_id, state.refuges)),
            ('unload', agent_id)]


def save_buried_civilian(state, agent_id, target_id):
    if state.type[target_id] == 'CIVILIAN':
        return [('rescue', agent_id, target_id),
                ('carry_injured', agent_id, target_id)]
    else:
        return False


def save_buried_agent(state, agent_id, target_id):
    return [('rescue', agent_id, target_id)]


def ambulance_act(state, agent_id):
    if state.type[agent_id] != 'AMBULANCE_TEAM':
        return False
    if state.buried:
        target_id = get_closest_from_set(state, agent_id, state.buried)
        return [('move', agent_id, state.position[target_id]),
                ('save', agent_id, target_id)]
    if state.injured:
        target_id = get_closest_from_set(state, agent_id, state.injured)
        return [('move', agent_id, state.position[target_id]),
                ('carry', agent_id, target_id)]
    else:
        return [('explore', agent_id)]


##################################################
#
#    Methods available for the fire brigade
#
##################################################


def refill_at_nearest_hydrant(state, agent_id):
    if state.hydrants:
        return [('move', agent_id, get_closest_from_set(state, agent_id, state.hydrants)),
                ('refill', agent_id)]
    return False


def refill_at_nearest_refuge(state, agent_id):
    if state.refuges:
        return [('move', agent_id, get_closest_from_set(state, agent_id, state.refuges)),
                ('refill', agent_id)]
    return False


def fire_act(state, agent_id):
    if state.type[agent_id] != "FIRE_BRIGADE":
        return False
    if state.water[agent_id] < state.max_water_power:
        return [('refill_at_nearest', agent_id)]
    if state.fire_buildings:
        target_id = get_closest_from_set(state, agent_id, state.fire_buildings)

        return [('move_until_in_range', agent_id, target_id, state.max_water_range),
                ('extinguish', agent_id, target_id, state.max_water_power)]

        # if in_range(state, agent_id, target_id, state.max_water_range):
        #     return [('extinguish', agent_id, target_id, state.max_water_power)]
        # else:
        #     return [('move_until_in_range', agent_id, target_id, state.max_water_range)]
    else:
        return [('explore', agent_id)]


##################################################
#
#    Methods available for the police force
#
##################################################


def go_to_entity_id_and_clear(state, agent_id, entity_id):
    if state.type[agent_id] != 'POLICE_FORCE':
        return False
    entity_pos = state.position[entity_id]
    entity_loc = state.location[entity_id]
    return [('clear_road', agent_id, entity_pos, entity_loc.x, entity_loc.y),
            ('clear_current_area', agent_id)]


def free_nearest_agent(state, agent_id):
    if state.type[agent_id] != 'POLICE_FORCE':
        return False

    if len(state.stuck_agents) == 0:
        return []

    closest_agent = get_closest_from_set(state, agent_id, state.stuck_agents)
    return [('go_to_entity_id_and_clear', agent_id, closest_agent)]


def free_nearest_civilian(state, agent_id):
    if state.type[agent_id] != "POLICE_FORCE":
        return False

    if len(state.stuck_civilians) == 0:
        return []

    closest_civilian = get_closest_from_set(state, agent_id, state.stuck_civilians)
    return [('go_to_entity_id_and_clear', agent_id, closest_civilian)]


def unclear_nearest_building(state, agent_id, road_clear_set):
    if state.type[agent_id] != "POLICE_FORCE":
        return False

    if len(road_clear_set) == 0:
        return []

    closest_road = get_closest_from_set(state, agent_id, road_clear_set)

    return [('go_to_entity_id_and_clear', agent_id, closest_road)]


def free_all_agents(state, agent_id):
    if state.type[agent_id] != 'POLICE_FORCE' or len(state.stuck_agents) == 0:
        return False
    plan = []
    for i in range(len(state.stuck_agents)):
        plan.append(('free_nearest_agent', agent_id))

    return plan


def free_all_civilians(state, agent_id):
    if state.type[agent_id] != 'POLICE_FORCE' or len(state.stuck_civilians) == 0:
        return False
    plan = []
    for i in range(len(state.stuck_civilians)):
        plan.append(('free_nearest_civilian', agent_id))

    return plan


def unclear_paths_to_all_buildings(state, agent_id, road_set):
    if state.type[agent_id] != "POLICE_FORCE" or not road_set:
        return False

    plan = []

    for i in range(len(state.stuck_civilians)):
        plan.append(('unclear_nearest_building', agent_id, road_set))

    return plan


def clear_closest_blockade(state, agent_id):
    blockade_id, blockade_line = get_closest_from_set_of_blockades(state, agent_id, state.blockades_to_apexes_map)
    agent_location = state.location[agent_id]
    end_point = find_a_perpendicular_given_a_point_and_a_line(agent_location, blockade_line[0], blockade_line[1])
    return [('move_to_point', agent_id, state.position[blockade_id], end_point.x, end_point.y),
            ('clear_current_area', agent_id)]


def police_act(state, agent_id):
    if state.type[agent_id] != "POLICE_FORCE":
        return False

    if state.stuck_agents:
        return [('free_all_agents', agent_id)]
    elif state.stuck_civilians:
        return [('free_all_civilians', agent_id)]

    road_set = find_clear_roads_near_buildings(state)

    if road_set:
        return [('unclear_paths_to_all_buildings', agent_id, road_set)]

    elif state.blockades_to_apexes_map:

        return [('clear_closest_blockade', agent_id)]

    else:
        return [('explore', agent_id)]

