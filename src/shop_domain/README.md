
This directory contains domain and problem files to generate SHOP plans for the RoboRescue connector.

These files can be run via the java-shop-server and locally.

Either option requires running code from the shop-connector, which require compiling that code and setting the path.

Running the java-shop-server
============================

- Create a symlink called jshop2_server_dir the location where the shop server is installed (usually shop-connector-git/src/java-shop-connector/jar)
- Run the script update_server.sh and follow the instructions
- Run "java -jar jshop2-server.jar" from that jar directory.

Running locally
===============

To compile locally using the make file, you must compile via make the project in shop-connector-git/src/jshop2/.
- Create a symlink called jshop_dir to the location where jshop is installed (usually shop-connector-git/src/jshop2)
- run 'setup_shop.sh' file.
- run the export command produced by 'setup_shop.sh'
- make the appropriate target for your desired problem


Files in this directory
=======================

Makefile - the make file to compile and run "domain.lisp" and "problem.lisp", which can be symlinked to any of the following:

domain_roboresecue_full.lisp - the latest full domain for the minecraft connector
problem_[test].lisp - the problem file for test


