(defproblem problem roborescue 
  (
   (type_brigade brigade_1)
   (available brigade_1)

   (type_building building_1)
   (burning building_1)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_shelter)
   (type_shelter placeholder_shelter)
   
   (placeholder placeholder_civilian)
   (type_civilian placeholder_shelter)
   
   (placeholder placeholder_brigade)
   (type_brigade placeholder_brigade)
   
   )
  ((douse_fire building_1))
)
